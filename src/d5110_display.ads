pragma Discard_Names;
pragma Restrictions (No_Enumeration_Maps);
pragma Normalize_Scalars;
pragma Restrictions (No_Exception_Propagation);
pragma Restrictions (No_Finalization);
pragma Restrictions (No_Tasking);
pragma Restrictions (No_Protected_Types);
pragma Restrictions (No_Delay);
pragma Restrictions (No_Recursion);
pragma Restrictions (No_Allocators);
pragma Restrictions (No_Dispatch);
pragma Restrictions (No_Implicit_Dynamic_Code);
pragma Restrictions (No_Secondary_Stack);
pragma Restrictions (No_Access_Subprograms);
pragma Restrictions (Static_Storage_Size);
pragma Restrictions (Simple_Barriers);
pragma Restrictions (No_Exceptions);
pragma Restrictions (No_Calendar);
pragma Restrictions (No_Obsolescent_Features);
pragma Restrictions (No_Io);
pragma Restrictions (No_Reentrancy);
pragma Static_Elaboration_Desired;

with Gpio;
with Interfaces;
-- @summary
--    Unit Name:	D5110_Display
--
--    Contained Units:
--			Init		(procedure)
--			Write		(procedure)
--			Write_Char	(procedure)
--			Write_String	(procedure)
--			Clear		(procedure)
--			SetXY		(procedure)
--			Line		(procedure)
--			Rectangle	(procedure)
--			Circle		(procedure)
--			Graphic		(procedure)
--
--   Description:
--   This unit contains subprograms to write characters, strings and
--   furthermore to draw simple forms and bitmaps to the D5110 display.
--   e.g. for main program snippet:
--
--	D5110_Display.Init;
--	D5110_Display.Graphic(graphic_maps.Ada_G);
--------------------------------------------------------------------------------
--	- Author: Marcus Br.
--	- Date	: 28/04/2019 (start of development)
--------------------------------------------------------------------------------
package D5110_Display is

--------------------------------------------------------------------------------
-- Datatypes --
--------------------------------------------------------------------------------

   subtype D_COMMAND_T is Interfaces.Unsigned_8;

   -- bitmaps for characters (ASCII code)
   type DISPLAY_CHARACTER_TA is array (0 .. 95, 0 .. 4) of D_COMMAND_T;
   for DISPLAY_CHARACTER_TA'Component_Size use 8;
   pragma Pack (DISPLAY_CHARACTER_TA);

   -- bitmap type for images 504 * 8 bit
   type MAP_5110_TA  is array (0 .. 503) of Interfaces.Unsigned_8;
   for MAP_5110_TA'Component_Size use 8;
   pragma Pack(MAP_5110_TA);

   -- type for pixel coordinates
   subtype NOKIA5110_DIMX_T is Natural range 0 .. 83;
   subtype NOKIA5110_DIMY_T is Natural range 0 .. 47;

--------------------------------------------------------------------------------
-- Constants --
--------------------------------------------------------------------------------

   Max_X : constant NOKIA5110_DIMX_T := 83;
   Max_Y : constant NOKIA5110_DIMY_T := 47;

--------------------------------------------------------------------------------
-- Subprograms --
--------------------------------------------------------------------------------

   procedure Write (DC : Gpio.PINVOLTAGE_T; Data : in D_COMMAND_T);
   -- Writes Data or Command to the Display.
   pragma Export_Procedure (Internal => Write, External => "Write");
   pragma Inline_Always (Write);

   procedure Write_Char (C : in Character);
   -- Writes character on the display.
   pragma Export_Procedure (Internal => Write_Char, External => "Write_Char");
   pragma Inline_Always (Write_Char);

   procedure Write_String (S : in String);
   -- Writes string on the display.
   pragma Export_Procedure
     (Internal => Write_String,
      External => "Write_String");
   pragma Inline_Always (Write_String);

   procedure Clear;
   -- Clear the display.
   pragma Export_Procedure (Internal => Clear, External => "Clear");
   pragma Inline_Always (Clear);


   procedure SetXY (X : in Interfaces.Unsigned_8; Y : in Interfaces.Unsigned_8);
   -- Set drawing cursor at X(0..83) and Y(0..47).
   pragma Export_Procedure (Internal => SetXY, External => "SetXY");
   pragma Inline_Always (SetXY);


   procedure PutPixel (X : in NOKIA5110_DIMX_T; Y : in NOKIA5110_DIMX_T);
    -- Set pixel black at X(0..83) and Y(0..47).
   pragma Export_Procedure (Internal => PutPixel, External => "PutPixel");
   pragma Inline_Always (PutPixel);

   procedure Update;
   -- Update the display content.
   pragma Export_Procedure (Internal => Update, External => "Update");
   pragma Inline_Always (Update);

   procedure Init;
   -- Display Init - setting GPIO Pins.
   pragma Export_Procedure (Internal => Init, External => "Init");
   pragma Inline_Always (Init);


   procedure Line
     (X1 : in NOKIA5110_DIMX_T;
      Y1 : in NOKIA5110_DIMY_T;
      X2 : in NOKIA5110_DIMX_T;
      Y2 : in NOKIA5110_DIMY_T);
   -- Draws a line on the display.
   pragma Export_Procedure (Internal => Line, External => "Line");
   pragma Inline_Always (Line);

   procedure Rectangle
     (X  : in NOKIA5110_DIMX_T;
      Y  : in NOKIA5110_DIMY_T;
      Dx : in NOKIA5110_DIMX_T;
      Dy : in NOKIA5110_DIMY_T);
   -- Draws a rectangle on the display.
   pragma Export_Procedure (Internal => Rectangle, External => "Rectangle");
   pragma Inline_Always (Rectangle);

   procedure Circle
     (X : in NOKIA5110_DIMX_T;
      Y : in NOKIA5110_DIMY_T;
      R :    Positive);
   -- Draws a circle on the display.
   pragma Export_Procedure (Internal => Circle, External => "Circle");
   pragma Inline_Always (Circle);

   procedure Graphic(G : in MAP_5110_TA);
   -- Draws a bitmap of type MAP_5110_TA on the display.
   pragma Export_Procedure (Internal => Graphic, External => "Graphic");
   pragma Inline_Always (Graphic);


private
   -- datatype represents the display buffer
   type DISPLAY_BUFFER_TA is array (0 .. 83, 0 .. 5) of D_COMMAND_T;
   for DISPLAY_BUFFER_TA'Component_Size use 8;
   pragma Pack (DISPLAY_BUFFER_TA);
end D5110_Display;
