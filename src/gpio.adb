pragma Discard_Names;
pragma Restrictions (No_Enumeration_Maps);
pragma Normalize_Scalars;
pragma Restrictions (No_Exception_Propagation);
pragma Restrictions (No_Finalization);
pragma Restrictions (No_Tasking);
pragma Restrictions (No_Protected_Types);
pragma Restrictions (No_Delay);
pragma Restrictions (No_Recursion);
pragma Restrictions (No_Allocators);
pragma Restrictions (No_Dispatch);
pragma Restrictions (No_Implicit_Dynamic_Code);
pragma Restrictions (No_Secondary_Stack);
pragma Restrictions (No_Access_Subprograms);
pragma Restrictions (Static_Storage_Size);
pragma Restrictions (Simple_Barriers);
pragma Restrictions (No_Exceptions);
pragma Restrictions (No_Calendar);
pragma Restrictions (No_Obsolescent_Features);
pragma Restrictions (No_Io);
pragma Restrictions (No_Reentrancy);
pragma Static_Elaboration_Desired;


package body Gpio is

   LED_GPIO_BIT : constant Positive := 16; -- Bit 16 of GPCLR0/GPSET0

   ------------------------------------------------------
   -- SET GPIO FUNCTION (INPUT, OUTPUT,A5,A4,A0,A1,A2,A3)
   ------------------------------------------------------
   procedure Set_GPIOF (Pin : in Natural; Func : in Unsigned_32) is
      Temp : Unsigned_32;
   begin
      if (Pin >= 0) and (Pin < 10) then
         Temp    := not (Shift_Left (7, Pin * 3));
         Bcm2835.GPIO_Reg.GPFSEL0 := Bcm2835.GPIO_Reg.GPFSEL0 and Temp;
         Temp    := Shift_Left (Func, Pin * 3);
         Bcm2835.GPIO_Reg.GPFSEL0 := Bcm2835.GPIO_Reg.GPFSEL0 or Temp;
      end if;
      if (Pin >= 10) and (Pin < 20) then
         Temp    := not (Shift_Left (7, (Pin - 10) * 3));
         Bcm2835.GPIO_Reg.GPFSEL1 := Bcm2835.GPIO_Reg.GPFSEL1 and Temp;
         Temp    := Shift_Left (Func, (Pin - 10) * 3);
         Bcm2835.GPIO_Reg.GPFSEL1 := Bcm2835.GPIO_Reg.GPFSEL1 or Temp;
      end if;
      if (Pin >= 20) and (Pin < 30) then
         Temp    := not (Shift_Left (7, (Pin - 20) * 3));
         Bcm2835.GPIO_Reg.GPFSEL2 := Bcm2835.GPIO_Reg.GPFSEL2 and Temp;
         Temp    := Shift_Left (Func, (Pin - 20) * 3);
         Bcm2835.GPIO_Reg.GPFSEL2 := Bcm2835.GPIO_Reg.GPFSEL2 or Temp;
      end if;
      if (Pin >= 40) and (Pin <= 49) then
         Temp    := not (Shift_Left (7, (Pin - 40) * 3));
         Bcm2835.GPIO_Reg.GPFSEL4 := Bcm2835.GPIO_Reg.GPFSEL4 and Temp;
         Temp    := Shift_Left (Func, (Pin - 40) * 3);
         Bcm2835.GPIO_Reg.GPFSEL4 := Bcm2835.GPIO_Reg.GPFSEL4 or Temp;
      end if;
   end Set_GPIOF;
   --  --------------------------------------------------
   --  SET GPIO VOLTAGE (LOW,HIGH)
   --  --------------------------------------------------
   procedure Set_Volt (Pin : in Natural; Func : in PINVOLTAGE_T) is
   begin
      if Pin < 32 then
         if Func = HIGH then
            Bcm2835.GPIO_Reg.GPSET0 := Bcm2835.GPIO_Reg.GPSET0 or (2**Pin);
         elsif Func = LOW then
            Bcm2835.GPIO_Reg.GPCLR0 := Bcm2835.GPIO_Reg.GPCLR0 or (2**Pin);
         end if;
      else
         if Func = HIGH then
            Bcm2835.GPIO_Reg.GPSET1 := Bcm2835.GPIO_Reg.GPSET1 or (2**Pin);
         elsif Func = LOW then
            Bcm2835.GPIO_Reg.GPCLR1 := Bcm2835.GPIO_Reg.GPCLR1 or (2**Pin);
         end if;
      end if;

   end Set_Volt;
   -----------------------------------------------------
   -- DELAYS T clock cycles
   ------------------------------------------------------
   procedure Wait (T : in Unsigned_64) is
      TEMP : Unsigned_64 := Bcm2835.S_TIMER.C;
   begin
      Bcm2835.S_TIMER.C := 0;
      while True loop
         if abs (Bcm2835.S_TIMER.C - TEMP) = T then
            exit;
         end if;
      end loop;
   end Wait;
   ---------------------------------------------------
   -- Blink ACT LED n-times ; pause T clock cycles between blink --
   ---------------------------------------------------
   procedure Blink (N : in Natural; T : in Unsigned_64) is
   begin
      Set_GPIOF ( LED_GPIO_BIT, OUTPUT);
      for I in 1 .. N loop
         Set_Volt ( LED_GPIO_BIT, LOW);
         Wait (T);
         Set_Volt ( LED_GPIO_BIT, HIGH);
         Wait (T);
      end loop;
   end Blink;
   -----------------------------
   --         SPI             --
   -----------------------------
   -- GPIO23: D/C
   -- GPIO24: RST
   -- GPIO26: GND

   -- GPIO7 : SPIO_CE1
   -- GPIO8 : SPIO_CE0
   -- GPIO9 : SPIO_MISO

   -- GPIO10 : SPIO_MOSI
   -- GPIO11 : SPIO_CLK
   procedure Init_SPI is
   begin
      --Set Pins to Alternate 0
      Set_GPIOF (7, A0);
      Set_GPIOF (8, A0);
      Set_GPIOF (9, A0);
      Set_GPIOF (10, A0);
      Set_GPIOF (11, A0);
      Set_GPIOF (27, OUTPUT); -- D/C
      --Clear TX and RX fifos
      Bcm2835.SPI_R.CS := Bcm2835.SPI_R.CS or 16#30#;

      --Set CS pins polarity to low,CPOL = 0, CPHA = 0
      Bcm2835.SPI_R.CS := Bcm2835.SPI_R.CS and (not (16#E0314C#));

      --Set Clock Divider to 128 = HEX 80
      Bcm2835.SPI_R.CLK := 16#200#;

      --Set Bit 0,1 LOW -- CHIP SELECT 0
      Bcm2835.SPI_R.CS := Bcm2835.SPI_R.CS and 16#FFFFFFFC#;

   end Init_SPI;
end Gpio;
