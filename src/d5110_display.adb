pragma Discard_Names;
pragma Restrictions (No_Enumeration_Maps);
pragma Normalize_Scalars;
pragma Restrictions (No_Exception_Propagation);
pragma Restrictions (No_Finalization);
pragma Restrictions (No_Tasking);
pragma Restrictions (No_Protected_Types);
pragma Restrictions (No_Delay);
pragma Restrictions (No_Recursion);
pragma Restrictions (No_Allocators);
pragma Restrictions (No_Dispatch);
pragma Restrictions (No_Implicit_Dynamic_Code);
pragma Restrictions (No_Secondary_Stack);
pragma Restrictions (No_Access_Subprograms);
pragma Restrictions (Static_Storage_Size);
pragma Restrictions (Simple_Barriers);
pragma Restrictions (No_Exceptions);
pragma Restrictions (No_Calendar);
pragma Restrictions (No_Obsolescent_Features);
pragma Restrictions (No_Io);
pragma Restrictions (No_Reentrancy);
pragma Static_Elaboration_Desired;

with Gpio;
with Graphic_Maps;
with Interfaces;
use Interfaces;

package body D5110_Display is

   ---------------------------
   -- Display Framebuffer
   ---------------------------
   FRAME_BUFFER : DISPLAY_BUFFER_TA := (others => (others => 16#1#));

   ---------------------------
   -- Display Write  8 Bit
   ---------------------------

   procedure Write (DC : Gpio.PINVOLTAGE_T; Data : in D_COMMAND_T) is
      Temp : Interfaces.Unsigned_8;
   begin
      -- Set D/C Pin
      Gpio.Set_Volt (27, DC);
      -- CS
      Gpio.Set_Volt (4, Gpio.LOW);
      Gpio.Wait (5);

      for I in 0 .. 7 loop
         Temp := Data and (Interfaces.Shift_Left (1, 7 - I));
         if Temp > 0 then
            Gpio.Set_Volt (10, Gpio.HIGH);
         else
            Gpio.Set_Volt (10, Gpio.LOW);
         end if;

         Gpio.Set_Volt (11, Gpio.HIGH); --CLK HIGH
         Gpio.Wait (5);
         Gpio.Set_Volt (11, Gpio.LOW); --CLK LOW
         Gpio.Wait (5);
         Gpio.Set_Volt (10, Gpio.LOW); -- MOSI LOW
      end loop;

      Gpio.Set_Volt (27, Gpio.LOW); -- D/C Low
      Gpio.Wait (10);
      Gpio.Set_Volt (4, Gpio.HIGH); -- CS HIGH
      Gpio.Wait (10);
      Gpio.Set_Volt (11, Gpio.LOW); -- CLK LOW
      Gpio.Wait (10);
   end Write;

   ---------------------------
   -- Display Write Chracter
   ---------------------------
   procedure Write_Char (C : in Character) is
   begin
      Write (Gpio.HIGH, 16#0#);
      for I in 0 .. 4 loop
         Write (Gpio.HIGH, Graphic_Maps.ASCII_Table (Character'Pos (C) - 16#20#, I));
      end loop;
      Write (Gpio.HIGH, 16#0#);
   end Write_Char;
   ---------------------------
   -- Display Write String
   ---------------------------
   procedure Write_String (S : in String) is
   begin
      for I in S'Range loop
         Write_Char (S (I));
      end loop;
   end Write_String;

   ---------------------------
   -- Display Set Pos XY
   ---------------------------
   procedure SetXY (X : in Interfaces.Unsigned_8; Y : in Interfaces.Unsigned_8) is
   begin
      Write (Gpio.LOW, 16#80# or X);
      Write (Gpio.LOW, 16#40# or Y);
   end SetXY;
   ---------------------------
   -- Display Clear
   ---------------------------
   procedure Clear is
   begin
      -- clear display ram
      for I in 1 .. (Max_X * Max_Y / 8) loop
         Write (Gpio.HIGH, 16#0#);
      end loop;
      SetXY (0, 0);
      -- clear frame buffer
      D5110_Display.FRAME_BUFFER := (others => (others => 16#0#));

   end Clear;
   ---------------------------------------------
   -- Display Set Pixel at X(0..83) and Y(0..47)
   ---------------------------------------------
   procedure PutPixel (X : in NOKIA5110_DIMX_T; Y : in NOKIA5110_DIMX_T) is
      Temp  : Interfaces.Unsigned_8 := 0;
      Tempy : Natural;
   begin
      if Y > 7 then
         Tempy := Y / 8;
      else
         Tempy := 0;
      end if;

      Temp                    := Interfaces.Shift_Left (16#1#, (Y mod 8));
      FRAME_BUFFER (X, Tempy) := FRAME_BUFFER (X, Tempy) or Temp;
   end PutPixel;
   ---------------------------
   -- Display Update
   ---------------------------
   procedure Update is
   begin
      SetXY (0, 0);
      for I in FRAME_BUFFER'Range (2) loop
         for J in FRAME_BUFFER'Range (1) loop
            Write (Gpio.HIGH, FRAME_BUFFER (J, I));
         end loop;
      end loop;
   end Update;
   ------------------------------------
   -- Draw Line - Bresenham Algorithmus
   ------------------------------------
   procedure Line
     (X1 : in NOKIA5110_DIMX_T;
      Y1 : in NOKIA5110_DIMY_T;
      X2 : in NOKIA5110_DIMX_T;
      Y2 : in NOKIA5110_DIMY_T)
   is
      Dx, X : NOKIA5110_DIMX_T := X2 - X1;
      Dy, Y : NOKIA5110_DIMY_T := Y2 - Y1;

      Adx : NOKIA5110_DIMX_T := abs Dx;
      Ady : NOKIA5110_DIMY_T := abs Dy;

      Pdx, Ddx : NOKIA5110_DIMX_T := 0;
      Pdy, Ddy : NOKIA5110_DIMY_T := 0;

      Sdx, Sdy, Fehler : Integer := 0;

      Es, El : NOKIA5110_DIMX_T;

   begin
      if Dx < 0 then
         Sdx := -1;
      elsif Dx > 0 then
         Sdx := 1;
      end if;

      if Dy < 0 then
         Sdy := -1;
      elsif Dy > 0 then
         Sdy := 1;
      end if;

      if Adx > Ady then
         Pdx := Sdx;
         Pdy := 0;
         Ddx := Sdx;
         Ddy := Sdy;
         Es  := Ady;
         El  := Adx;
      else
         Pdx := 0;
         Pdy := Sdy;
         Ddx := Sdx;
         Ddy := Sdy;
         Es  := Adx;
         El  := Ady;
      end if;
      X := X1;
      Y := Y1;

      PutPixel (X1, Y1);

      for I in 1 .. El loop
         Fehler := Fehler - Es; -- Aktualisierung Fehlerterm
         if Fehler < 0 then
            Fehler := Fehler + El; --Fehlerterm wieder positiv (>=0) machen
            X      := X + Ddx; -- Diagonalschritt
            Y      := Y + Ddy; -- Diagonalschritt
         else -- Schritt in schnelle Richtung
            X := X + Pdx; -- Parallelschritt
            Y := Y + Pdy; -- Parallelschritt
         end if;
         PutPixel (X, Y);
      end loop;
   end Line;

   ------------------------------------
   -- Draw Rectangle (X,Y,Dx,Dy)
   ------------------------------------
   procedure Rectangle
     (X  : in NOKIA5110_DIMX_T;
      Y  : in NOKIA5110_DIMY_T;
      Dx : in NOKIA5110_DIMX_T;
      Dy : in NOKIA5110_DIMY_T)
   is
   begin
      Line (X, Y, X + Dx, Y);
      Line (X, Y + Dy, X + Dx, Y + Dy);

      Line (X, Y, X, Y + Dy);
      Line (X + Dx, Y, X + Dx, Y + Dy);
   end Rectangle;

   ------------------------------------
   -- Draw Circle (X,Y,R)
   ------------------------------------
   procedure Circle
     (X : in NOKIA5110_DIMX_T;
      Y : in NOKIA5110_DIMY_T;
      R :    Positive)
   is
      TempX : Natural    := R;
      TempY : Natural    := 0;
      E     : Integer    := -R;
      DedX  : Interfaces.Unsigned_8 := 2 - Interfaces.Shift_Left (Interfaces.Unsigned_8 (R), 1);
      DedY  : Integer    := 1;
   begin
      PutPixel (X + R, Y);
      PutPixel (X, Y + R);
      PutPixel (X - R, Y);
      PutPixel (X, Y - R);
      while (TempX > TempY) loop
         TempY := TempY + 1;
         E     := E + DedY;
         DedY  := DedY + 2;
         if E > 0 then
            TempX := TempX - 1;
            E     := E + Integer (DedX);
            DedX  := DedX + 2;
         end if;

         PutPixel (X + TempX, Y + TempY);
         PutPixel (X + TempX, Y - TempY);
         PutPixel (X - TempX, Y + TempY);
         PutPixel (X - TempX, Y - TempY);
         PutPixel (X + TempY, Y + TempX);
         PutPixel (X + TempY, Y - TempX);
         PutPixel (X - TempY, Y + TempX);
         PutPixel (X - TempY, Y - TempX);

      end loop;

   end Circle;

   ---------------------------
   -- Display Init
   ---------------------------
   procedure Init is
   begin

      Gpio.Set_GPIOF (4, Gpio.OUTPUT); -- SCE

      Gpio.Set_GPIOF (9, Gpio.INPUT);  -- MISO
      Gpio.Set_GPIOF (10, Gpio.OUTPUT); -- MOSI
      Gpio.Set_GPIOF (11, Gpio.OUTPUT); -- CLK

      Gpio.Set_GPIOF (27, Gpio.OUTPUT); -- D/C
      Gpio.Wait (5);

      Gpio.Set_Volt (4, Gpio.HIGH); -- SCE HIGH
      Gpio.Set_Volt (11, Gpio.LOW); -- CLK LOW
      Gpio.Wait (5);

      Gpio.Set_GPIOF (18, Gpio.OUTPUT); -- RESET
      Gpio.Set_Volt (18, Gpio.LOW);
      Gpio.Wait (350);
      Gpio.Set_Volt (18, Gpio.HIGH);
      Gpio.Wait (350);
      Gpio.Set_Volt (11, Gpio.LOW); -- CLK LOW

      Write (Gpio.LOW, 16#21#); --C 0010 0001 Extended Instruction Set
      Write (Gpio.LOW, 16#90#); --C 1001 0001 Set Vop=16
      Write (Gpio.LOW, 16#20#); --C 0010 0000 Basic instruction set
      Write (Gpio.LOW, 16#C#);  --C 0000 1100 Display normal mode

      Clear;
   end Init;
   ---------------------------
   -- Display Paint Graphic
   ---------------------------
   procedure Graphic(G : in MAP_5110_TA) is
   begin
      D5110_Display.SetXY (0, 0);
      for I in FRAME_BUFFER'Range (2) loop
         for J in FRAME_BUFFER'Range (1) loop
            FRAME_BUFFER (J, I) := G (J + 84 * I);
         end loop;
      end loop;
      D5110_Display.Update;
   end Graphic;

end D5110_Display;
