pragma Discard_Names;
pragma Restrictions (No_Enumeration_Maps);
pragma Normalize_Scalars;
pragma Restrictions (No_Exception_Propagation);
pragma Restrictions (No_Finalization);
pragma Restrictions (No_Tasking);
pragma Restrictions (No_Protected_Types);
pragma Restrictions (No_Delay);
pragma Restrictions (No_Recursion);
pragma Restrictions (No_Allocators);
pragma Restrictions (No_Dispatch);
pragma Restrictions (No_Implicit_Dynamic_Code);
pragma Restrictions (No_Secondary_Stack);
pragma Restrictions (No_Access_Subprograms);
pragma Restrictions (Static_Storage_Size);
pragma Restrictions (Simple_Barriers);
pragma Restrictions (No_Exceptions);
pragma Restrictions (No_Calendar);
pragma Restrictions (No_Obsolescent_Features);
pragma Restrictions (No_Io);
pragma Restrictions (No_Reentrancy);
pragma Static_Elaboration_Desired;

with Interfaces;
use Interfaces;
with System;
with Bcm2835;


-- @summary
--    Unit Name:	Gpio
--
--    Contained Units:
--			Wait		(procedure)
--			Set_GPIOF	(procedure)
--			Set_Volt	(procedure)
--			Blink		(procedure)
--			Init_SPI	(procedure)
--
--   Description:
--   This unit contains subprograms to set a pins function and level.
--   Furthermore, there is the blink procedure for the ACT LED (Gpio 16).
--   Init_SPI initializes the pins for this sw interface.
--
--	GPIO23: D/C
-- 	GPIO24: RST
-- 	GPIO26: GND
-- 	GPIO7 : SPIO_CE1
-- 	GPIO8 : SPIO_CE0
-- 	GPIO9 : SPIO_MISO
-- 	GPIO10 : SPIO_MOSI
-- 	GPIO11 : SPIO_CLK
--------------------------------------------------------------------------------
--	- Author: Marcus Br.
--	- Date	: 28/04/2019 (start of development)
--------------------------------------------------------------------------------
package Gpio is
--------------------------------------------------------------------------------
-- Datatypes --
-------------------------------------------------------------------------------
   type PINVOLTAGE_T is (LOW, HIGH);
   for PINVOLTAGE_T use (LOW => 0, HIGH => 1);
   for PINVOLTAGE_T'Size use 1;


--------------------------------------------------------------------------------
-- Constants --
--------------------------------------------------------------------------------
   -- pin as input pin
   INPUT  : constant Unsigned_32 := 0;
   -- pin as output pin
   OUTPUT : constant Unsigned_32 := 1;
   -- pin alternate function 0
   A0     : constant Unsigned_32 := 4;
   -- pin alternate function 1
   A1     : constant Unsigned_32 := 5;
   -- pin alternate function 2
   A2     : constant Unsigned_32 := 6;
   -- pin alternate function 3
   A3     : constant Unsigned_32 := 7;
   -- pin alternate function 4
   A4     : constant Unsigned_32 := 3;
   -- pin alternate function 5
   A5     : constant Unsigned_32 := 2;


--------------------------------------------------------------------------------
-- Subprograms --
--------------------------------------------------------------------------------
   procedure Wait (T : in Unsigned_64);
   --  Wait T clock cycles
   --  @param T T number of clock cycles to do nothing
   pragma Export_Procedure (Internal => Wait, External => "Wait");

   procedure Set_GPIOF (Pin : in Natural; Func : in Unsigned_32);
   --  SET GPIO FUNCTION (INPUT, OUTPUT,A5,A4,A0,A1,A2,A3)
   --  Ref: GPIO Function Select Registers (GPFSELn)
   --  Ref: BCM2835 ARM Peripherals, p.91-94, 2012
   --  @param Pin 0..48
   --  @param Func INPUT,OUTPUT, A0..A5
   pragma Export_Procedure (Internal => Set_GPIOF, External => "Set_GPIOF");
   pragma Inline_Always (Set_GPIOF);

   procedure Set_Volt (Pin : in Natural; Func : in PINVOLTAGE_T);
   --  SET PIN VOLTAGE (LOW, HIGH)
   --  @param Pin 0..48
   --  @param Func LOW, HIGH
   pragma Export_Procedure (Internal => Set_Volt, External => "Set_Volt");

   procedure Blink (N : in Natural; T : in Unsigned_64);
   --  Blink the onboard ACT LED
   --  @param N N phases of LED ON - WAIT T - OFF - WAIT T -
   --  @param T T clock cycles LED ON
   pragma Export_Procedure (Internal => Blink, External => "Blink");

   procedure Init_SPI;
   -- Initialize the Serial Peripheral Interface
   pragma Export_Procedure (Internal => Init_SPI, External => "Init_SPI");

end Gpio;
