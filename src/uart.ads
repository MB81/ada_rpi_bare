pragma Discard_Names;
pragma Restrictions (No_Enumeration_Maps);
pragma Normalize_Scalars;
pragma Restrictions (No_Exception_Propagation);
pragma Restrictions (No_Finalization);
pragma Restrictions (No_Tasking);
pragma Restrictions (No_Protected_Types);
pragma Restrictions (No_Delay);
pragma Restrictions (No_Recursion);
pragma Restrictions (No_Allocators);
pragma Restrictions (No_Dispatch);
pragma Restrictions (No_Implicit_Dynamic_Code);
pragma Restrictions (No_Secondary_Stack);
pragma Restrictions (No_Access_Subprograms);
pragma Restrictions (Static_Storage_Size);
pragma Restrictions (Simple_Barriers);
pragma Restrictions (No_Exceptions);
pragma Restrictions (No_Calendar);
pragma Restrictions (No_Obsolescent_Features);
pragma Restrictions (No_Io);
pragma Restrictions (No_Reentrancy);
pragma Static_Elaboration_Desired;

with System;     use System;
with Interfaces; use Interfaces;
with Interfaces.C;
with Bcm2835;
with Gpio;
with Ada.Characters.Latin_1;
with Ada.Unchecked_Conversion;
-- @summary
--    Unit Name:	Uart
--
--    Datatypes:
--			UART_MAP	(record)
--			BIT_T		(natural)
--			B32_REG_T	(array)
--
--    Variables:
--			UART_M : UART_MAP;
--			FR_REG : B32_REG_T;
--
--    Contained Units:
--			Init		(procedure)
--			PutC		(procedure)
--			GetC		(procedure)
--			PutLine		(procedure)
--			NewLine		(procedure)
--			ToString	(procedure)
--    Description:
--    Parameter for serial port:
--    115200 baud, 8 bits, no parity, 1 stop bit and no flow control.
--    Pin layout:
--    GPIO 14 (UART TXD) goes to RXD of the controller
--    GPIO 15 (UART RXD) goes to TXD of the controller
--    GND goes to GND
--------------------------------------------------------------------------------
--	- Author: Marcus Br.
--	- Date	: 28/04/2019 (start of development)
--------------------------------------------------------------------------------
package Uart is
--------------------------------------------------------------------------------
-- Subprograms --
--------------------------------------------------------------------------------
   procedure Init;
   pragma Export_Procedure (Internal => Init, External => "Uart_Init");
   pragma Inline_Always (Init);

   procedure PutC(C : in Character);
   pragma Export_Procedure (Internal => PutC, External => "Uart_PutC");
   pragma Inline_Always (PutC);

   function GetC return Character;
   pragma Export_Function (Internal => GetC, External => "Uart_GetC");
   pragma Inline_Always (GetC);

   procedure PutLine(S : in String);
   pragma Export_Procedure (Internal => PutLine, External => "Uart_PutLine");
   pragma Inline_Always (PutLine);

   procedure NewLine;
   pragma Export_Procedure (Internal => NewLine, External => "Uart_NewLine");
   pragma Inline_Always (NewLine);

   procedure ToString (I : in Unsigned_32);
   pragma Export_Procedure (Internal => ToString, External => "Uart_ToString");
   pragma Inline_Always (ToString);
end Uart;
