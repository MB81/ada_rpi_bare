pragma Discard_Names;
pragma Restrictions (No_Enumeration_Maps);
pragma Normalize_Scalars;
pragma Restrictions (No_Exception_Propagation);
pragma Restrictions (No_Finalization);
pragma Restrictions (No_Tasking);
pragma Restrictions (No_Protected_Types);
pragma Restrictions (No_Delay);
pragma Restrictions (No_Recursion);
pragma Restrictions (No_Allocators);
pragma Restrictions (No_Dispatch);
pragma Restrictions (No_Implicit_Dynamic_Code);
pragma Restrictions (No_Secondary_Stack);
pragma Restrictions (No_Access_Subprograms);
pragma Restrictions (Static_Storage_Size);
pragma Restrictions (Simple_Barriers);
pragma Restrictions (No_Exceptions);
pragma Restrictions (No_Calendar);
pragma Restrictions (No_Obsolescent_Features);
pragma Restrictions (No_Io);
pragma Restrictions (No_Reentrancy);
pragma Static_Elaboration_Desired;



package body Uart is


   procedure Init is
   begin
      --  BCM2835 ARM Peripherals, p.185
      --  initialize Control Register
      Bcm2835.UART_REG.CR := 16#0#;
      --  GPIO 14 Alernate 0 , UART TXD
      Gpio.Set_GPIOF (14, Gpio.OUTPUT);
      Gpio.Set_GPIOF (14, Gpio.A0);
      --  GPIO 15 Alernate 0 , UART RXD
      Gpio.Set_GPIOF (15, Gpio.INPUT);
      Gpio.Set_GPIOF (15, Gpio.A0);

      --  BCM2835 ARM Peripherals, p.100
      --  disable Pull/Down
      Bcm2835.GPIO_Reg.GPPUD := 0;
      --  wait for changed
      Gpio.Wait (1000);
      --  BCM2835 ARM Peripherals, p.101
      --  set bit 14 and 15 to change GPIO 14 and 15
      Bcm2835.GPIO_Reg.GPPUDCLK0 := Bcm2835.GPIO_Reg.GPPUDCLK0 or 16#C000#;
      --  wait for changed
      Gpio.Wait (1000);
      --  set everything to 0
      Bcm2835.GPIO_Reg.GPPUDCLK0 := 0;

      --  BCM2835 ARM Peripherals, p.192
      --  interrupt clear register; set bit 0 to 10 to clear all
      Bcm2835.UART_REG.ICR  := 16#7FF#;
      --  BCM2835 ARM Peripherals, p.183
      --  baud rate divisor - integer part set to 1
      Bcm2835.UART_REG.IBRD := 1;
      --  baud rate divisor - fractional part set to 40
      Bcm2835.UART_REG.FBRD := 40;
      --  BCM2835 ARM Peripherals, p.184
      --  line control register - set bit 4,5,6 - enable FIFO, and 8bit words
      Bcm2835.UART_REG.LCRH := 16#70#;
      --  BCM2835 ARM Peripherals, p.185
      --  control register - set bit 0,8,9 - enable UART, TX enable, RX enable
      Bcm2835.UART_REG.CR   := 16#301#;
   end Init;

   procedure PutC (C : in Character) is
   begin

      while Bcm2835.FR_REG (6) = 1 loop
         Gpio.Wait (10);
      end loop;

      Bcm2835.UART_REG.DR := Character'Pos (C);
      Gpio.Wait (100);
   end PutC;

   procedure PutLine (S : in String) is
   begin
      for I in S'Range loop
         PutC (S (I));
      end loop;

      PutC (Ada.Characters.Latin_1.CR);
      PutC (Ada.Characters.Latin_1.LF);
      Gpio.Wait(100);
   end PutLine;

   function Integer_To_Character is new Ada.Unchecked_Conversion
     (Source => Unsigned_32,
      Target => Character);

   procedure ToString (I : in Unsigned_32) is
      Rb : Integer;
      Rc : Unsigned_32;
   begin
      Rb := 32;
      while (Rb > 0) loop
         Rb := Rb - 4;
         Rc := Shift_Right (I, Rb) and 16#F#;
         if (Rc > 9) then
            Rc := Rc + 16#37#;
         else
            Rc := Rc + 16#30#;
         end if;
         PutC (Integer_To_Character (Rc));
      end loop;
   end ToString;

   procedure NewLine is
   begin
      PutC (Ada.Characters.Latin_1.CR);
      PutC (Ada.Characters.Latin_1.LF);
   end NewLine;


   function GetC return Character is
   begin

      while Bcm2835.FR_REG (5) = 1 loop
         Gpio.Wait (10);
      end loop;
      return Character'Val (Bcm2835.UART_REG.DR);
   end GetC;

end Uart;
