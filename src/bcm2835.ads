pragma Discard_Names;
pragma Restrictions (No_Enumeration_Maps);
pragma Normalize_Scalars;
pragma Restrictions (No_Exception_Propagation);
pragma Restrictions (No_Finalization);
pragma Restrictions (No_Tasking);
pragma Restrictions (No_Protected_Types);
pragma Restrictions (No_Delay);
pragma Restrictions (No_Recursion);
pragma Restrictions (No_Allocators);
pragma Restrictions (No_Dispatch);
pragma Restrictions (No_Implicit_Dynamic_Code);
pragma Restrictions (No_Secondary_Stack);
pragma Restrictions (No_Access_Subprograms);
pragma Restrictions (Static_Storage_Size);
pragma Restrictions (Simple_Barriers);
pragma Restrictions (No_Exceptions);
pragma Restrictions (No_Calendar);
pragma Restrictions (No_Obsolescent_Features);
pragma Restrictions (No_Io);
pragma Restrictions (No_Reentrancy);
pragma Static_Elaboration_Desired;

with System; 
with Interfaces; 
use Interfaces;
-- @summary
--    Unit Name:	Bcm2835
--
--    Datatypes:
--			SYSTEM_TIMER_REG_TR	(record)
--			SPI_REG_TR		(record)
--			GPIO_REG_TR		(record)
--
--    Constants:
--			BCM2835_Peri_Base = 16#20000000#
--			BCM2835_GPIO_Base = 16#20200000#
--	
--    Variables:
--			GPIO_Reg 	: GPIO_REG_TR
--			SPI_R 		: SPI_REG_TR;
--			S_TIMER 	: SYSTEM_TIMER_REG_TR;
--
--   Description:
--   This unit contains the constants and memory mapped variables to address
--   the general purpose input/ouput pins and the timer of the BCM2835.	
--------------------------------------------------------------------------------
--	- Author: Marcus Br.
--	- Date	: 28/04/2019 (start of development)
--------------------------------------------------------------------------------
package Bcm2835 is

--------------------------------------------------------------------------------
-- Datatypes --
--------------------------------------------------------------------------------
   subtype BIT_T is Natural range 0 .. 1;
   
   type B32_REG_TA is array (1..32) of BIT_T;
--------------------------------------------------------------------------------	
-- SYSTEM TIMER REGISTER --
--------------------------------------------------------------------------------
   --  SYSTEM TIMER REGISTER
   --  @field CS System Timer Control/Status
   --  @field C System Timer Counter Lower 32 bits + Higher 32 bits
   --  @field C0 System Timer Compare 0
   --  @field C1 System Timer Compare 1
   --  @field C2 System Timer Compare 2
   --  @field C3 System Timer Compare 3
   type SYSTEM_TIMER_REG_TR is record
      CS : Interfaces.Unsigned_32;
      C  : Interfaces.Unsigned_64;
      C0 : Interfaces.Unsigned_32;
      C1 : Interfaces.Unsigned_32;
      C2 : Interfaces.Unsigned_32;
      C3 : Interfaces.Unsigned_32;
   end record;
   pragma Pack (SYSTEM_TIMER_REG_TR);

--------------------------------------------------------------------------------	
-- SPI REGISTER--
--------------------------------------------------------------------------------
   --  Serial Peripheral Interface REGISTER
   --  @field CS SPI Master Control and Status
   --  @field FIFO SPI Master TX and RX FIFOs
   --  @field CLK SPI Master Clock Divider
   --  @field DLEN SPI Master Data Length
   --  @field LTOH SPI LOSSI mode TOH
   --  @field DC SPI DMA DREQ Controls
   type SPI_REG_TR is record
      CS   : Interfaces.Unsigned_32;
      FIFO : Interfaces.Unsigned_32;
      CLK  : Interfaces.Unsigned_32;
      DLEN : Interfaces.Unsigned_32;
      LTOH : Interfaces.Unsigned_32;
      DC   : Interfaces.Unsigned_32;
   end record;
   pragma Pack (SPI_REG_TR);
--------------------------------------------------------------------------------	
-- UART REGISTER--
--------------------------------------------------------------------------------
   type UART_REG_TR is record
      DR     : Interfaces.Unsigned_32;
      RSRECR : Interfaces.Unsigned_32;
      FR     : Interfaces.Unsigned_32;
      ILPR   : Interfaces.Unsigned_32;
      IBRD   : Interfaces.Unsigned_32;
      FBRD   : Interfaces.Unsigned_32;
      LCRH   : Interfaces.Unsigned_32;
      CR     : Interfaces.Unsigned_32;
      IFLS   : Interfaces.Unsigned_32;
      IMSC   : Interfaces.Unsigned_32;
      RIS    : Interfaces.Unsigned_32;
      MIS    : Interfaces.Unsigned_32;
      ICR    : Interfaces.Unsigned_32;
      DMACR  : Interfaces.Unsigned_32;
      ITCR   : Interfaces.Unsigned_32;
      ITIP   : Interfaces.Unsigned_32;
      ITOP   : Interfaces.Unsigned_32;
      TDR    : Interfaces.Unsigned_32;
   end record;   
   pragma Pack (UART_REG_TR);
------------------------------------------------------------------------	
-- GPIO REGISTER--
------------------------------------------------------------------------
   --  General Purpose IO Pin REGISTER
   type GPIO_REG_TR is record
      GPFSEL0   : Interfaces.Unsigned_32;
      GPFSEL1   : Interfaces.Unsigned_32;
      GPFSEL2   : Interfaces.Unsigned_32;
      GPFSEL3   : Interfaces.Unsigned_32;
      GPFSEL4   : Interfaces.Unsigned_32;
      GPFSEL5   : Interfaces.Unsigned_32;
      Res1      : Interfaces.Unsigned_32;
      GPSET0    : Interfaces.Unsigned_32;
      GPSET1    : Interfaces.Unsigned_32;
      Res2      : Interfaces.Unsigned_32;
      GPCLR0    : Interfaces.Unsigned_32;
      GPCLR1    : Interfaces.Unsigned_32;
      Res3      : Interfaces.Unsigned_32;
      GPLEV0    : Interfaces.Unsigned_32;
      GPLEV1    : Interfaces.Unsigned_32;
      Res4      : Interfaces.Unsigned_32;
      GPEDS0    : Interfaces.Unsigned_32;
      GPEDS1    : Interfaces.Unsigned_32;
      Res5      : Interfaces.Unsigned_32;
      GPREN0    : Interfaces.Unsigned_32;
      GPREN1    : Interfaces.Unsigned_32;
      Res6      : Interfaces.Unsigned_32;
      GPFEN0    : Interfaces.Unsigned_32;
      GPFEN1    : Interfaces.Unsigned_32;
      Res7      : Interfaces.Unsigned_32;
      GPHEN0    : Interfaces.Unsigned_32;
      GPHEN1    : Interfaces.Unsigned_32;
      Res8      : Interfaces.Unsigned_32;
      GPLEN0    : Interfaces.Unsigned_32;
      GPLEN1    : Interfaces.Unsigned_32;
      Res9      : Interfaces.Unsigned_32;
      GPAREN0   : Interfaces.Unsigned_32;
      GPAREN1   : Interfaces.Unsigned_32;
      Res10     : Interfaces.Unsigned_32;
      GPAFEN0   : Interfaces.Unsigned_32;
      GPAFEN1   : Interfaces.Unsigned_32;
      Res11     : Interfaces.Unsigned_32;
      GPPUD     : Interfaces.Unsigned_32;
      GPPUDCLK0 : Interfaces.Unsigned_32;
      GPPUDCLK1 : Interfaces.Unsigned_32;
      Res12     : Interfaces.Unsigned_32;
   end record;
   pragma Pack (GPIO_REG_TR);
   
--------------------------------------------------------------------------------
-- Constants --
--------------------------------------------------------------------------------

   -- Base address of peripherals
   BCM2835_Peri_Base : constant Interfaces.Unsigned_32 := 16#20000000#;

   -- Base address of General Purpose IO Pins
   BCM2835_GPIO_Base : constant Interfaces.Unsigned_32 := 16#20200000#;
   
--------------------------------------------------------------------------------
-- Variables --
--------------------------------------------------------------------------------

   GPIO_Reg : GPIO_REG_TR;
   for GPIO_Reg'Address use System'To_Address (BCM2835_GPIO_Base);
   pragma Import (Ada, GPIO_Reg);
   
   SPI_R : SPI_REG_TR;
   for SPI_R'Address use System'To_Address (BCM2835_GPIO_Base + 16#4000#);
   for SPI_R'Size use 192;
   pragma Import (Ada, SPI_R);
   
   S_TIMER : SYSTEM_TIMER_REG_TR;
   for S_TIMER'Address use System'To_Address (BCM2835_Peri_Base + 16#3000#);
   for S_TIMER'Size use 224;
   pragma Import (Ada, S_TIMER);
   
   UART_REG : UART_REG_TR;
   for UART_REG'Address use System'To_Address (BCM2835_GPIO_Base + 16#1000#);
   pragma Import (Ada, UART_REG);

   FR_REG : B32_REG_TA;
   for FR_REG'Address use System'To_Address (BCM2835_GPIO_Base + 16#1018#);
   pragma Import (Ada, FR_REG);
  

end Bcm2835;
