pragma Discard_Names;
pragma Restrictions (No_Enumeration_Maps);
pragma Normalize_Scalars;
pragma Restrictions (No_Exception_Propagation);
pragma Restrictions (No_Finalization);
pragma Restrictions (No_Tasking);
pragma Restrictions (No_Protected_Types);
pragma Restrictions (No_Delay);
pragma Restrictions (No_Recursion);
pragma Restrictions (No_Allocators);
pragma Restrictions (No_Dispatch);
pragma Restrictions (No_Implicit_Dynamic_Code);
pragma Restrictions (No_Secondary_Stack);
pragma Restrictions (No_Access_Subprograms);
pragma Restrictions (Static_Storage_Size);
pragma Restrictions (Simple_Barriers);
pragma Restrictions (No_Exceptions);
pragma Restrictions (No_Calendar);
pragma Restrictions (No_Obsolescent_Features);
pragma Restrictions (No_Io);
pragma Restrictions (No_Reentrancy);
pragma Static_Elaboration_Desired;

with Gpio;
with D5110_Display;
with Graphic_Maps;
with Uart;
procedure Main is

begin
   -- I'm alive
   Gpio.Blink (5, 100000);

   -----------------------------
   --         SPI             --
   -----------------------------
   -- GPIO23: D/C
   -- GPIO24: RST
   -- GPIO26: GND

   -- GPIO7 : SPIO_CE1
   -- GPIO8 : SPIO_CE0
   -- GPIO9 : SPIO_MISO

   -- GPIO10 : SPIO_MOSI
   -- GPIO11 : SPIO_CLK
   D5110_Display.Init;

   D5110_Display.Graphic(Graphic_Maps.Ada_G);

   -----------------------------
   --        UART             --
   -----------------------------
   -- GPIO 14 Alernate 0 , UART TXD goes to RX of controller
   -- GPIO 15 Alernate 0 , UART RXD goes to TX of controller
   -- GND goes to GND
   Uart.Init;

   Uart.PutLine("Hello RPI Ada!");
end Main;
