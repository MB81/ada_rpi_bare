GNATdoc.SourceFile = {
  "kind": "code",
  "children": [
    {
      "kind": "line",
      "number": 1,
      "children": [
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "pragma"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Discard_Names"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 2,
      "children": [
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "pragma"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Restrictions"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "("
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "No_Enumeration_Maps"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ")"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 3,
      "children": [
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "pragma"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Normalize_Scalars"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 4,
      "children": [
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "pragma"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Restrictions"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "("
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "No_Exception_Propagation"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ")"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 5,
      "children": [
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "pragma"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Restrictions"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "("
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "No_Finalization"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ")"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 6,
      "children": [
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "pragma"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Restrictions"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "("
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "No_Tasking"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ")"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 7,
      "children": [
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "pragma"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Restrictions"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "("
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "No_Protected_Types"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ")"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 8,
      "children": [
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "pragma"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Restrictions"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "("
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "No_Delay"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ")"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 9,
      "children": [
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "pragma"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Restrictions"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "("
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "No_Recursion"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ")"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 10,
      "children": [
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "pragma"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Restrictions"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "("
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "No_Allocators"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ")"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 11,
      "children": [
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "pragma"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Restrictions"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "("
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "No_Dispatch"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ")"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 12,
      "children": [
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "pragma"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Restrictions"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "("
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "No_Implicit_Dynamic_Code"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ")"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 13,
      "children": [
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "pragma"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Restrictions"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "("
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "No_Secondary_Stack"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ")"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 14,
      "children": [
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "pragma"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Restrictions"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "("
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "No_Access_Subprograms"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ")"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 15,
      "children": [
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "pragma"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Restrictions"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "("
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Static_Storage_Size"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ")"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 16,
      "children": [
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "pragma"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Restrictions"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "("
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Simple_Barriers"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ")"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 17,
      "children": [
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "pragma"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Restrictions"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "("
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "No_Exceptions"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ")"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 18,
      "children": [
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "pragma"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Restrictions"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "("
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "No_Calendar"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ")"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 19,
      "children": [
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "pragma"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Restrictions"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "("
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "No_Obsolescent_Features"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ")"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 20,
      "children": [
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "pragma"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Restrictions"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "("
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "No_Io"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ")"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 21,
      "children": [
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "pragma"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Restrictions"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "("
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "No_Reentrancy"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ")"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 22,
      "children": [
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "pragma"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Static_Elaboration_Desired"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 23,
      "children": [
      ]
    },
    {
      "kind": "line",
      "number": 24,
      "children": [
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "with"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "System"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        }
      ]
    },
    {
      "kind": "line",
      "number": 25,
      "children": [
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "with"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        }
      ]
    },
    {
      "kind": "line",
      "number": 26,
      "children": [
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "use"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 27,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "-- @summary"
        }
      ]
    },
    {
      "kind": "line",
      "number": 28,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--    Unit Name:\tBcm2835"
        }
      ]
    },
    {
      "kind": "line",
      "number": 29,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--"
        }
      ]
    },
    {
      "kind": "line",
      "number": 30,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--    Datatypes:"
        }
      ]
    },
    {
      "kind": "line",
      "number": 31,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--\t\t\tSYSTEM_TIMER_REG_TR\t(record)"
        }
      ]
    },
    {
      "kind": "line",
      "number": 32,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--\t\t\tSPI_REG_TR\t\t(record)"
        }
      ]
    },
    {
      "kind": "line",
      "number": 33,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--\t\t\tGPIO_REG_TR\t\t(record)"
        }
      ]
    },
    {
      "kind": "line",
      "number": 34,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--"
        }
      ]
    },
    {
      "kind": "line",
      "number": 35,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--    Constants:"
        }
      ]
    },
    {
      "kind": "line",
      "number": 36,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--\t\t\tBCM2835_Peri_Base = 16#20000000#"
        }
      ]
    },
    {
      "kind": "line",
      "number": 37,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--\t\t\tBCM2835_GPIO_Base = 16#20200000#"
        }
      ]
    },
    {
      "kind": "line",
      "number": 38,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--\t"
        }
      ]
    },
    {
      "kind": "line",
      "number": 39,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--    Variables:"
        }
      ]
    },
    {
      "kind": "line",
      "number": 40,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--\t\t\tGPIO_Reg \t: GPIO_REG_TR"
        }
      ]
    },
    {
      "kind": "line",
      "number": 41,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--\t\t\tSPI_R \t\t: SPI_REG_TR;"
        }
      ]
    },
    {
      "kind": "line",
      "number": 42,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--\t\t\tS_TIMER \t: SYSTEM_TIMER_REG_TR;"
        }
      ]
    },
    {
      "kind": "line",
      "number": 43,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--"
        }
      ]
    },
    {
      "kind": "line",
      "number": 44,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--   Description:"
        }
      ]
    },
    {
      "kind": "line",
      "number": 45,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--   This unit contains the constants and memory mapped variables to address"
        }
      ]
    },
    {
      "kind": "line",
      "number": 46,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--   the general purpose input/ouput pins and the timer of the BCM2835.\t"
        }
      ]
    },
    {
      "kind": "line",
      "number": 47,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--------------------------------------------------------------------------------"
        }
      ]
    },
    {
      "kind": "line",
      "number": 48,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--\t- Author: Marcus Br."
        }
      ]
    },
    {
      "kind": "line",
      "number": 49,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--\t- Date\t: 28/04/2019 (start of development)"
        }
      ]
    },
    {
      "kind": "line",
      "number": 50,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--------------------------------------------------------------------------------"
        }
      ]
    },
    {
      "kind": "line",
      "number": 51,
      "children": [
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "package"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Bcm2835",
          "href": "docs/bcm2835___spec.html#L51C9"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "is"
        }
      ]
    },
    {
      "kind": "line",
      "number": 52,
      "children": [
      ]
    },
    {
      "kind": "line",
      "number": 53,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--------------------------------------------------------------------------------"
        }
      ]
    },
    {
      "kind": "line",
      "number": 54,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "-- Datatypes --"
        }
      ]
    },
    {
      "kind": "line",
      "number": 55,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--------------------------------------------------------------------------------"
        }
      ]
    },
    {
      "kind": "line",
      "number": 56,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "subtype"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "BIT_T",
          "href": "docs/bcm2835___spec.html#L56C12"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "is"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Natural"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "range"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "number",
          "text": "0"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ".."
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "number",
          "text": "1"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 57,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        }
      ]
    },
    {
      "kind": "line",
      "number": 58,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "type"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "B32_REG_TA",
          "href": "docs/bcm2835___spec.html#L58C9"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "is"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "array"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "("
        },
        {
          "kind": "span",
          "cssClass": "number",
          "text": "1"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ".."
        },
        {
          "kind": "span",
          "cssClass": "number",
          "text": "32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ")"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "of"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "BIT_T",
          "href": "docs/bcm2835___spec.html#L56C12"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 59,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--------------------------------------------------------------------------------\t"
        }
      ]
    },
    {
      "kind": "line",
      "number": 60,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "-- SYSTEM TIMER REGISTER --"
        }
      ]
    },
    {
      "kind": "line",
      "number": 61,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--------------------------------------------------------------------------------"
        }
      ]
    },
    {
      "kind": "line",
      "number": 62,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--  SYSTEM TIMER REGISTER"
        }
      ]
    },
    {
      "kind": "line",
      "number": 63,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--  @field CS System Timer Control/Status"
        }
      ]
    },
    {
      "kind": "line",
      "number": 64,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--  @field C System Timer Counter Lower 32 bits + Higher 32 bits"
        }
      ]
    },
    {
      "kind": "line",
      "number": 65,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--  @field C0 System Timer Compare 0"
        }
      ]
    },
    {
      "kind": "line",
      "number": 66,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--  @field C1 System Timer Compare 1"
        }
      ]
    },
    {
      "kind": "line",
      "number": 67,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--  @field C2 System Timer Compare 2"
        }
      ]
    },
    {
      "kind": "line",
      "number": 68,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--  @field C3 System Timer Compare 3"
        }
      ]
    },
    {
      "kind": "line",
      "number": 69,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "type"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "SYSTEM_TIMER_REG_TR",
          "href": "docs/bcm2835___spec.html#L69C9"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "is"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "record"
        }
      ]
    },
    {
      "kind": "line",
      "number": 70,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "CS",
          "href": "docs/bcm2835___spec.html#L70C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 71,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "C",
          "href": "docs/bcm2835___spec.html#L71C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "  "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_64"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 72,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "C0",
          "href": "docs/bcm2835___spec.html#L72C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 73,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "C1",
          "href": "docs/bcm2835___spec.html#L73C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 74,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "C2",
          "href": "docs/bcm2835___spec.html#L74C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 75,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "C3",
          "href": "docs/bcm2835___spec.html#L75C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 76,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "end"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "record"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";",
          "href": "docs/bcm2835___spec.html#L69C9"
        }
      ]
    },
    {
      "kind": "line",
      "number": 77,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "pragma"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Pack"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "("
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "SYSTEM_TIMER_REG_TR",
          "href": "docs/bcm2835___spec.html#L69C9"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ")"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 78,
      "children": [
      ]
    },
    {
      "kind": "line",
      "number": 79,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--------------------------------------------------------------------------------\t"
        }
      ]
    },
    {
      "kind": "line",
      "number": 80,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "-- SPI REGISTER--"
        }
      ]
    },
    {
      "kind": "line",
      "number": 81,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--------------------------------------------------------------------------------"
        }
      ]
    },
    {
      "kind": "line",
      "number": 82,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--  Serial Peripheral Interface REGISTER"
        }
      ]
    },
    {
      "kind": "line",
      "number": 83,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--  @field CS SPI Master Control and Status"
        }
      ]
    },
    {
      "kind": "line",
      "number": 84,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--  @field FIFO SPI Master TX and RX FIFOs"
        }
      ]
    },
    {
      "kind": "line",
      "number": 85,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--  @field CLK SPI Master Clock Divider"
        }
      ]
    },
    {
      "kind": "line",
      "number": 86,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--  @field DLEN SPI Master Data Length"
        }
      ]
    },
    {
      "kind": "line",
      "number": 87,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--  @field LTOH SPI LOSSI mode TOH"
        }
      ]
    },
    {
      "kind": "line",
      "number": 88,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--  @field DC SPI DMA DREQ Controls"
        }
      ]
    },
    {
      "kind": "line",
      "number": 89,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "type"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "SPI_REG_TR",
          "href": "docs/bcm2835___spec.html#L89C9"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "is"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "record"
        }
      ]
    },
    {
      "kind": "line",
      "number": 90,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "CS",
          "href": "docs/bcm2835___spec.html#L90C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 91,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "FIFO",
          "href": "docs/bcm2835___spec.html#L91C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 92,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "CLK",
          "href": "docs/bcm2835___spec.html#L92C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "  "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 93,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "DLEN",
          "href": "docs/bcm2835___spec.html#L93C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 94,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "LTOH",
          "href": "docs/bcm2835___spec.html#L94C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 95,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "DC",
          "href": "docs/bcm2835___spec.html#L95C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 96,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "end"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "record"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";",
          "href": "docs/bcm2835___spec.html#L89C9"
        }
      ]
    },
    {
      "kind": "line",
      "number": 97,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "pragma"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Pack"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "("
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "SPI_REG_TR",
          "href": "docs/bcm2835___spec.html#L89C9"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ")"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 98,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--------------------------------------------------------------------------------\t"
        }
      ]
    },
    {
      "kind": "line",
      "number": 99,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "-- UART REGISTER--"
        }
      ]
    },
    {
      "kind": "line",
      "number": 100,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--------------------------------------------------------------------------------"
        }
      ]
    },
    {
      "kind": "line",
      "number": 101,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "type"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "UART_REG_TR",
          "href": "docs/bcm2835___spec.html#L101C9"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "is"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "record"
        }
      ]
    },
    {
      "kind": "line",
      "number": 102,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "DR",
          "href": "docs/bcm2835___spec.html#L102C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "     "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 103,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "RSRECR",
          "href": "docs/bcm2835___spec.html#L103C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 104,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "FR",
          "href": "docs/bcm2835___spec.html#L104C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "     "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 105,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "ILPR",
          "href": "docs/bcm2835___spec.html#L105C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 106,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "IBRD",
          "href": "docs/bcm2835___spec.html#L106C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 107,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "FBRD",
          "href": "docs/bcm2835___spec.html#L107C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 108,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "LCRH",
          "href": "docs/bcm2835___spec.html#L108C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 109,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "CR",
          "href": "docs/bcm2835___spec.html#L109C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "     "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 110,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "IFLS",
          "href": "docs/bcm2835___spec.html#L110C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 111,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "IMSC",
          "href": "docs/bcm2835___spec.html#L111C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 112,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "RIS",
          "href": "docs/bcm2835___spec.html#L112C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "    "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 113,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "MIS",
          "href": "docs/bcm2835___spec.html#L113C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "    "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 114,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "ICR",
          "href": "docs/bcm2835___spec.html#L114C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "    "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 115,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "DMACR",
          "href": "docs/bcm2835___spec.html#L115C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "  "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 116,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "ITCR",
          "href": "docs/bcm2835___spec.html#L116C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 117,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "ITIP",
          "href": "docs/bcm2835___spec.html#L117C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 118,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "ITOP",
          "href": "docs/bcm2835___spec.html#L118C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 119,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "TDR",
          "href": "docs/bcm2835___spec.html#L119C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "    "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 120,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "end"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "record"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";",
          "href": "docs/bcm2835___spec.html#L101C9"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        }
      ]
    },
    {
      "kind": "line",
      "number": 121,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "pragma"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Pack"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "("
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "UART_REG_TR",
          "href": "docs/bcm2835___spec.html#L101C9"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ")"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 122,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "------------------------------------------------------------------------\t"
        }
      ]
    },
    {
      "kind": "line",
      "number": 123,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "-- GPIO REGISTER--"
        }
      ]
    },
    {
      "kind": "line",
      "number": 124,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "------------------------------------------------------------------------"
        }
      ]
    },
    {
      "kind": "line",
      "number": 125,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--  General Purpose IO Pin REGISTER"
        }
      ]
    },
    {
      "kind": "line",
      "number": 126,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--  There are 54 general-purpose I/O (GPIO) lines split into two banks. All GPIO pins have at"
        }
      ]
    },
    {
      "kind": "line",
      "number": 127,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--  least two alternative functions within BCM. The alternate functions are usually peripheral IO"
        }
      ]
    },
    {
      "kind": "line",
      "number": 128,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--  and  a  single  peripheral  may  appear  in  each  bank  to  allow  flexibility  on  the  choice  of  IO"
        }
      ]
    },
    {
      "kind": "line",
      "number": 129,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--  voltage."
        }
      ]
    },
    {
      "kind": "line",
      "number": 130,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "type"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "GPIO_REG_TR",
          "href": "docs/bcm2835___spec.html#L130C9"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "is"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "record"
        }
      ]
    },
    {
      "kind": "line",
      "number": 131,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "GPFSEL0",
          "href": "docs/bcm2835___spec.html#L131C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 132,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "GPFSEL1",
          "href": "docs/bcm2835___spec.html#L132C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 133,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "GPFSEL2",
          "href": "docs/bcm2835___spec.html#L133C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 134,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "GPFSEL3",
          "href": "docs/bcm2835___spec.html#L134C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 135,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "GPFSEL4",
          "href": "docs/bcm2835___spec.html#L135C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 136,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "GPFSEL5",
          "href": "docs/bcm2835___spec.html#L136C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 137,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Res1",
          "href": "docs/bcm2835___spec.html#L137C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 138,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "GPSET0",
          "href": "docs/bcm2835___spec.html#L138C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "    "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 139,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "GPSET1",
          "href": "docs/bcm2835___spec.html#L139C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "    "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 140,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Res2",
          "href": "docs/bcm2835___spec.html#L140C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 141,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "GPCLR0",
          "href": "docs/bcm2835___spec.html#L141C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "    "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 142,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "GPCLR1",
          "href": "docs/bcm2835___spec.html#L142C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "    "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 143,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Res3",
          "href": "docs/bcm2835___spec.html#L143C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 144,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "GPLEV0",
          "href": "docs/bcm2835___spec.html#L144C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "    "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 145,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "GPLEV1",
          "href": "docs/bcm2835___spec.html#L145C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "    "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 146,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Res4",
          "href": "docs/bcm2835___spec.html#L146C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 147,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "GPEDS0",
          "href": "docs/bcm2835___spec.html#L147C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "    "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 148,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "GPEDS1",
          "href": "docs/bcm2835___spec.html#L148C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "    "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 149,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Res5",
          "href": "docs/bcm2835___spec.html#L149C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 150,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "GPREN0",
          "href": "docs/bcm2835___spec.html#L150C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "    "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 151,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "GPREN1",
          "href": "docs/bcm2835___spec.html#L151C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "    "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 152,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Res6",
          "href": "docs/bcm2835___spec.html#L152C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 153,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "GPFEN0",
          "href": "docs/bcm2835___spec.html#L153C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "    "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 154,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "GPFEN1",
          "href": "docs/bcm2835___spec.html#L154C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "    "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 155,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Res7",
          "href": "docs/bcm2835___spec.html#L155C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 156,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "GPHEN0",
          "href": "docs/bcm2835___spec.html#L156C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "    "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 157,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "GPHEN1",
          "href": "docs/bcm2835___spec.html#L157C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "    "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 158,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Res8",
          "href": "docs/bcm2835___spec.html#L158C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 159,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "GPLEN0",
          "href": "docs/bcm2835___spec.html#L159C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "    "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 160,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "GPLEN1",
          "href": "docs/bcm2835___spec.html#L160C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "    "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 161,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Res9",
          "href": "docs/bcm2835___spec.html#L161C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 162,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "GPAREN0",
          "href": "docs/bcm2835___spec.html#L162C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 163,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "GPAREN1",
          "href": "docs/bcm2835___spec.html#L163C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 164,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Res10",
          "href": "docs/bcm2835___spec.html#L164C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "     "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 165,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "GPAFEN0",
          "href": "docs/bcm2835___spec.html#L165C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 166,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "GPAFEN1",
          "href": "docs/bcm2835___spec.html#L166C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 167,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Res11",
          "href": "docs/bcm2835___spec.html#L167C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "     "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 168,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "GPPUD",
          "href": "docs/bcm2835___spec.html#L168C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "     "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 169,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "GPPUDCLK0",
          "href": "docs/bcm2835___spec.html#L169C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 170,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "GPPUDCLK1",
          "href": "docs/bcm2835___spec.html#L170C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 171,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "      "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Res12",
          "href": "docs/bcm2835___spec.html#L171C7"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": "     "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 172,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "end"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "record"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";",
          "href": "docs/bcm2835___spec.html#L130C9"
        }
      ]
    },
    {
      "kind": "line",
      "number": 173,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "pragma"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Pack"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "("
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "GPIO_REG_TR",
          "href": "docs/bcm2835___spec.html#L130C9"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ")"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 174,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        }
      ]
    },
    {
      "kind": "line",
      "number": 175,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--------------------------------------------------------------------------------"
        }
      ]
    },
    {
      "kind": "line",
      "number": 176,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "-- Constants --"
        }
      ]
    },
    {
      "kind": "line",
      "number": 177,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--------------------------------------------------------------------------------"
        }
      ]
    },
    {
      "kind": "line",
      "number": 178,
      "children": [
      ]
    },
    {
      "kind": "line",
      "number": 179,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "-- Base address of peripherals"
        }
      ]
    },
    {
      "kind": "line",
      "number": 180,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "BCM2835_Peri_Base",
          "href": "docs/bcm2835___spec.html#L180C4"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "constant"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":="
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "number",
          "text": "16"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "#"
        },
        {
          "kind": "span",
          "cssClass": "number",
          "text": "20000000"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "#"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 181,
      "children": [
      ]
    },
    {
      "kind": "line",
      "number": 182,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "-- Base address of General Purpose IO Pins"
        }
      ]
    },
    {
      "kind": "line",
      "number": 183,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "BCM2835_GPIO_Base",
          "href": "docs/bcm2835___spec.html#L183C4"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "constant"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Interfaces.Unsigned_32"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":="
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "number",
          "text": "16"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "#"
        },
        {
          "kind": "span",
          "cssClass": "number",
          "text": "20200000"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "#"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 184,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        }
      ]
    },
    {
      "kind": "line",
      "number": 185,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--------------------------------------------------------------------------------"
        }
      ]
    },
    {
      "kind": "line",
      "number": 186,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "-- Variables --"
        }
      ]
    },
    {
      "kind": "line",
      "number": 187,
      "children": [
        {
          "kind": "span",
          "cssClass": "comment",
          "text": "--------------------------------------------------------------------------------"
        }
      ]
    },
    {
      "kind": "line",
      "number": 188,
      "children": [
      ]
    },
    {
      "kind": "line",
      "number": 189,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "GPIO_Reg",
          "href": "docs/bcm2835___spec.html#L189C4"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "GPIO_REG_TR",
          "href": "docs/bcm2835___spec.html#L130C9"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 190,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "for"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "GPIO_Reg"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "'"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Address"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "use"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "System"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "'"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "To_Address"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "("
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "BCM2835_GPIO_Base"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ")"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 191,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "pragma"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Import"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "("
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Ada"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ","
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "GPIO_Reg"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ")"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 192,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        }
      ]
    },
    {
      "kind": "line",
      "number": 193,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "SPI_R",
          "href": "docs/bcm2835___spec.html#L193C4"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "SPI_REG_TR",
          "href": "docs/bcm2835___spec.html#L89C9"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 194,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "for"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "SPI_R"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "'"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Address"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "use"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "System"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "'"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "To_Address"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "("
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "BCM2835_GPIO_Base"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "+"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "number",
          "text": "16"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "#"
        },
        {
          "kind": "span",
          "cssClass": "number",
          "text": "4000"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "#"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ")"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 195,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "for"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "SPI_R"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "'"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Size"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "use"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "number",
          "text": "192"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 196,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "pragma"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Import"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "("
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Ada"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ","
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "SPI_R"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ")"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 197,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        }
      ]
    },
    {
      "kind": "line",
      "number": 198,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "S_TIMER",
          "href": "docs/bcm2835___spec.html#L198C4"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "SYSTEM_TIMER_REG_TR",
          "href": "docs/bcm2835___spec.html#L69C9"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 199,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "for"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "S_TIMER"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "'"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Address"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "use"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "System"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "'"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "To_Address"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "("
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "BCM2835_Peri_Base"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "+"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "number",
          "text": "16"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "#"
        },
        {
          "kind": "span",
          "cssClass": "number",
          "text": "3000"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "#"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ")"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 200,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "for"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "S_TIMER"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "'"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Size"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "use"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "number",
          "text": "224"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 201,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "pragma"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Import"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "("
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Ada"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ","
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "S_TIMER"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ")"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 202,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        }
      ]
    },
    {
      "kind": "line",
      "number": 203,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "UART_REG",
          "href": "docs/bcm2835___spec.html#L203C4"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "UART_REG_TR",
          "href": "docs/bcm2835___spec.html#L101C9"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 204,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "for"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "UART_REG"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "'"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Address"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "use"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "System"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "'"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "To_Address"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "("
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "BCM2835_GPIO_Base"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "+"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "number",
          "text": "16"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "#"
        },
        {
          "kind": "span",
          "cssClass": "number",
          "text": "1000"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "#"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ")"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 205,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "pragma"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Import"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "("
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Ada"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ","
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "UART_REG"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ")"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 206,
      "children": [
      ]
    },
    {
      "kind": "line",
      "number": 207,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "FR_REG",
          "href": "docs/bcm2835___spec.html#L207C4"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ":"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "B32_REG_TA",
          "href": "docs/bcm2835___spec.html#L58C9"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 208,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "for"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "FR_REG"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "'"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Address"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "use"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "System"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "'"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "To_Address"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "("
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "BCM2835_GPIO_Base"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "+"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "number",
          "text": "16"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "#"
        },
        {
          "kind": "span",
          "cssClass": "number",
          "text": "1018"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "#"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ")"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 209,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "   "
        },
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "pragma"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Import"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "("
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Ada"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ","
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "FR_REG"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ")"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    },
    {
      "kind": "line",
      "number": 210,
      "children": [
        {
          "kind": "span",
          "cssClass": "text",
          "text": "  "
        }
      ]
    },
    {
      "kind": "line",
      "number": 211,
      "children": [
      ]
    },
    {
      "kind": "line",
      "number": 212,
      "children": [
        {
          "kind": "span",
          "cssClass": "keyword",
          "text": "end"
        },
        {
          "kind": "span",
          "cssClass": "text",
          "text": " "
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": "Bcm2835"
        },
        {
          "kind": "span",
          "cssClass": "identifier",
          "text": ";"
        }
      ]
    }
  ],
  "label": "bcm2835.ads"
};