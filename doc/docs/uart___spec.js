GNATdoc.Documentation = {
  "label": "Uart",
  "qualifier": "",
  "summary": [
    {
      "kind": "paragraph",
      "cssClass": "preformatted",
      "children": [
        {
          "kind": "span",
          "text": "    Unit Name:\tUart\n\n    Datatypes:\n\t\t\tUART_MAP\t(record)\n\t\t\tBIT_T\t\t(natural)\n\t\t\tB32_REG_T\t(array)\n\n    Variables:\n\t\t\tUART_M : UART_MAP;\n\t\t\tFR_REG : B32_REG_T;\n\n    Contained Units:\n\t\t\tInit\t\t(procedure)\n\t\t\tPutC\t\t(procedure)\n\t\t\tGetC\t\t(procedure)\n\t\t\tPutLine\t\t(procedure)\n\t\t\tNewLine\t\t(procedure)\n\t\t\tToString\t(procedure)\n    Description:\n    Parameter for serial port:\n    115200 baud, 8 bits, no parity, 1 stop bit and no flow control.\n    Pin layout:\n    GPIO 14 (UART TXD) goes to RXD of the controller\n    GPIO 15 (UART RXD) goes to TXD of the controller\n    GND goes to GND\n------------------------------------------------------------------------------\n\t- Author: Marcus Br.\n\t- Date\t: 28/04/2019 (start of development)\n------------------------------------------------------------------------------\n\n"
        }
      ]
    }
  ],
  "description": [
    {
      "kind": "paragraph",
      "cssClass": "preformatted",
      "children": [
        {
          "kind": "span",
          "text": "\n"
        }
      ]
    }
  ],
  "entities": [
    {
      "entities": [
        {
          "label": "GetC",
          "qualifier": "",
          "line": 73,
          "column": 13,
          "src": "srcs/uart.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 73,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "function"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "GetC",
                      "href": "docs/uart___spec.html#L73C13"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "return"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Character"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": "\n"
                }
              ]
            }
          ]
        },
        {
          "label": "Init",
          "qualifier": "",
          "line": 65,
          "column": 14,
          "src": "srcs/uart.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 65,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "procedure"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Init",
                      "href": "docs/uart___spec.html#L65C14"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": "------------------------------------------------------------------------------\n Subprograms --\n------------------------------------------------------------------------------\n\n"
                }
              ]
            }
          ]
        },
        {
          "label": "NewLine",
          "qualifier": "",
          "line": 81,
          "column": 14,
          "src": "srcs/uart.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 81,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "procedure"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "NewLine",
                      "href": "docs/uart___spec.html#L81C14"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": "\n"
                }
              ]
            }
          ]
        },
        {
          "label": "PutC",
          "qualifier": "",
          "line": 69,
          "column": 14,
          "src": "srcs/uart.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 69,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "procedure"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "PutC",
                      "href": "docs/uart___spec.html#L69C14"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "("
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "C",
                      "href": "docs/uart___spec.html#L69C19"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "in"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Character"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ")"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": "\n"
                }
              ]
            }
          ],
          "parameters": [
            {
              "label": "C",
              "line": 69,
              "column": 19,
              "type": {
                "label": "Character"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "label": "PutLine",
          "qualifier": "",
          "line": 77,
          "column": 14,
          "src": "srcs/uart.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 77,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "procedure"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "PutLine",
                      "href": "docs/uart___spec.html#L77C14"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "("
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "S",
                      "href": "docs/uart___spec.html#L77C22"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "in"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "String"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ")"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": "\n"
                }
              ]
            }
          ],
          "parameters": [
            {
              "label": "S",
              "line": 77,
              "column": 22,
              "type": {
                "label": "String"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "label": "ToString",
          "qualifier": "",
          "line": 85,
          "column": 14,
          "src": "srcs/uart.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 85,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "procedure"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "ToString",
                      "href": "docs/uart___spec.html#L85C14"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "("
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "I",
                      "href": "docs/uart___spec.html#L85C24"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "in"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ")"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": "\n"
                }
              ]
            }
          ],
          "parameters": [
            {
              "label": "I",
              "line": 85,
              "column": 24,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            }
          ]
        }
      ],
      "label": "Subprograms"
    }
  ]
};