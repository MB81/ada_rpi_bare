GNATdoc.Documentation = {
  "label": "Spi",
  "qualifier": "",
  "summary": [
  ],
  "description": [
  ],
  "entities": [
    {
      "entities": [
        {
          "label": "InitInterface",
          "qualifier": "",
          "line": 26,
          "column": 14,
          "src": "srcs/spi.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 26,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "procedure"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "InitInterface",
                      "href": "docs/spi___spec.html#L26C14"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": "\n"
                }
              ]
            }
          ]
        }
      ],
      "label": "Subprograms"
    }
  ]
};