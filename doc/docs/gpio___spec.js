GNATdoc.Documentation = {
  "label": "Gpio",
  "qualifier": "",
  "summary": [
    {
      "kind": "paragraph",
      "cssClass": "preformatted",
      "children": [
        {
          "kind": "span",
          "text": "    Unit Name:\tGpio\n\n    Contained Units:\n\t\t\tWait\t\t(procedure)\n\t\t\tSet_GPIOF\t(procedure)\n\t\t\tSet_Volt\t(procedure)\n\t\t\tBlink\t\t(procedure)\n\t\t\tInit_SPI\t(procedure)\n\n   Description:\n   This unit contains subprograms to set a pins function and level.\n   Furthermore, there is the blink procedure for the ACT LED (Gpio 16).\n   Init_SPI initializes the pins for this sw interface.\n\n\tGPIO23: D/C\n \tGPIO24: RST\n \tGPIO26: GND\n \tGPIO7 : SPIO_CE1\n \tGPIO8 : SPIO_CE0\n \tGPIO9 : SPIO_MISO\n \tGPIO10 : SPIO_MOSI\n \tGPIO11 : SPIO_CLK\n------------------------------------------------------------------------------\n\t- Author: Marcus Br.\n\t- Date\t: 28/04/2019 (start of development)\n------------------------------------------------------------------------------\n\n"
        }
      ]
    }
  ],
  "description": [
    {
      "kind": "paragraph",
      "cssClass": "preformatted",
      "children": [
        {
          "kind": "span",
          "text": "\n"
        }
      ]
    }
  ],
  "entities": [
    {
      "entities": [
        {
          "label": "A0",
          "qualifier": "",
          "line": 74,
          "column": 4,
          "src": "srcs/gpio.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 74,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "A0"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "     "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "constant"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":="
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "number",
                      "text": "4"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": " pin alternate function 0\n\n"
                }
              ]
            }
          ]
        },
        {
          "label": "A1",
          "qualifier": "",
          "line": 76,
          "column": 4,
          "src": "srcs/gpio.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 76,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "A1"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "     "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "constant"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":="
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "number",
                      "text": "5"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": " pin alternate function 1\n\n"
                }
              ]
            }
          ]
        },
        {
          "label": "A2",
          "qualifier": "",
          "line": 78,
          "column": 4,
          "src": "srcs/gpio.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 78,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "A2"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "     "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "constant"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":="
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "number",
                      "text": "6"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": " pin alternate function 2\n\n"
                }
              ]
            }
          ]
        },
        {
          "label": "A3",
          "qualifier": "",
          "line": 80,
          "column": 4,
          "src": "srcs/gpio.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 80,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "A3"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "     "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "constant"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":="
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "number",
                      "text": "7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": " pin alternate function 3\n\n"
                }
              ]
            }
          ]
        },
        {
          "label": "A4",
          "qualifier": "",
          "line": 82,
          "column": 4,
          "src": "srcs/gpio.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 82,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "A4"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "     "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "constant"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":="
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "number",
                      "text": "3"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": " pin alternate function 4\n\n"
                }
              ]
            }
          ]
        },
        {
          "label": "A5",
          "qualifier": "",
          "line": 84,
          "column": 4,
          "src": "srcs/gpio.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 84,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "A5"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "     "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "constant"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":="
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "number",
                      "text": "2"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": " pin alternate function 5\n\n"
                }
              ]
            }
          ]
        },
        {
          "label": "INPUT",
          "qualifier": "",
          "line": 70,
          "column": 4,
          "src": "srcs/gpio.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 70,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "INPUT"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "  "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "constant"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":="
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "number",
                      "text": "0"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": "------------------------------------------------------------------------------\n Constants --\n------------------------------------------------------------------------------\n pin as input pin\n\n"
                }
              ]
            }
          ]
        },
        {
          "label": "OUTPUT",
          "qualifier": "",
          "line": 72,
          "column": 4,
          "src": "srcs/gpio.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 72,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "OUTPUT"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "constant"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":="
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "number",
                      "text": "1"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": " pin as output pin\n\n"
                }
              ]
            }
          ]
        }
      ],
      "label": "Constants and variables"
    },
    {
      "entities": [
        {
          "label": "PINVOLTAGE_T",
          "qualifier": "",
          "line": 61,
          "column": 9,
          "src": "srcs/gpio.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 61,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "type"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "PINVOLTAGE_T",
                      "href": "docs/gpio___spec.html#L61C9"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "is"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "("
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "LOW",
                      "href": "docs/gpio___spec.html#L61C26"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ","
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "HIGH",
                      "href": "docs/gpio___spec.html#L61C31"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ")"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";",
                      "href": "docs/gpio___spec.html#L61C9"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": "------------------------------------------------------------------------------\n Datatypes --\n-----------------------------------------------------------------------------\n\n"
                }
              ]
            }
          ],
          "literals": [
            {
              "label": "LOW",
              "line": 61,
              "column": 26,
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "HIGH",
              "line": 61,
              "column": 31,
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            }
          ]
        }
      ],
      "label": "Simple types"
    },
    {
      "entities": [
        {
          "label": "Blink",
          "qualifier": "",
          "line": 110,
          "column": 14,
          "src": "srcs/gpio.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 110,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "procedure"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Blink",
                      "href": "docs/gpio___spec.html#L110C14"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "("
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "N",
                      "href": "docs/gpio___spec.html#L110C21"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "in"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Natural"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "T",
                      "href": "docs/gpio___spec.html#L110C37"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "in"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Unsigned_64"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ")"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": "  Blink the onboard ACT LED\n \n"
                }
              ]
            }
          ],
          "parameters": [
            {
              "label": "N",
              "line": 110,
              "column": 21,
              "type": {
                "label": "Natural"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "  N phases of LED ON - WAIT T - OFF - WAIT T -\n \n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "T",
              "line": 110,
              "column": 37,
              "type": {
                "label": "Unsigned_64"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "T clock cycles LED ON\n"
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "label": "Init_SPI",
          "qualifier": "",
          "line": 116,
          "column": 14,
          "src": "srcs/gpio.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 116,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "procedure"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Init_SPI",
                      "href": "docs/gpio___spec.html#L116C14"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": " Initialize the Serial Peripheral Interface\n\n"
                }
              ]
            }
          ]
        },
        {
          "label": "Set_GPIOF",
          "qualifier": "",
          "line": 95,
          "column": 14,
          "src": "srcs/gpio.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 95,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "procedure"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Set_GPIOF",
                      "href": "docs/gpio___spec.html#L95C14"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "("
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Pin",
                      "href": "docs/gpio___spec.html#L95C25"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "in"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Natural"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Func",
                      "href": "docs/gpio___spec.html#L95C43"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "in"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ")"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": "  SET GPIO FUNCTION (INPUT, OUTPUT,A5,A4,A0,A1,A2,A3)\n  Ref: GPIO Function Select Registers (GPFSELn)\n  Ref: BCM2835 ARM Peripherals, p.91-94, 2012\n \n"
                }
              ]
            }
          ],
          "parameters": [
            {
              "label": "Pin",
              "line": 95,
              "column": 25,
              "type": {
                "label": "Natural"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "  0..48\n \n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "Func",
              "line": 95,
              "column": 43,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "INPUT,OUTPUT, A0..A5\n"
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "label": "Set_Volt",
          "qualifier": "",
          "line": 104,
          "column": 14,
          "src": "srcs/gpio.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 104,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "procedure"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Set_Volt",
                      "href": "docs/gpio___spec.html#L104C14"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "("
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Pin",
                      "href": "docs/gpio___spec.html#L104C24"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "in"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Natural"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Func",
                      "href": "docs/gpio___spec.html#L104C42"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "in"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "PINVOLTAGE_T",
                      "href": "docs/gpio___spec.html#L61C9"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ")"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": "  SET PIN VOLTAGE (LOW, HIGH)\n \n"
                }
              ]
            }
          ],
          "parameters": [
            {
              "label": "Pin",
              "line": 104,
              "column": 24,
              "type": {
                "label": "Natural"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "  0..48\n \n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "Func",
              "line": 104,
              "column": 42,
              "type": {
                "label": "Gpio.PINVOLTAGE_T",
                "docHref": "docs/gpio___spec.html#L61C9"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "LOW, HIGH\n"
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "label": "Wait",
          "qualifier": "",
          "line": 90,
          "column": 14,
          "src": "srcs/gpio.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 90,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "procedure"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Wait",
                      "href": "docs/gpio___spec.html#L90C14"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "("
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "T",
                      "href": "docs/gpio___spec.html#L90C20"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "in"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Unsigned_64"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ")"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": "------------------------------------------------------------------------------\n Subprograms --\n------------------------------------------------------------------------------\n\n"
                }
              ]
            }
          ],
          "parameters": [
            {
              "label": "T",
              "line": 90,
              "column": 20,
              "type": {
                "label": "Unsigned_64"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            }
          ]
        }
      ],
      "label": "Subprograms"
    }
  ]
};