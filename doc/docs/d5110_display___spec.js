GNATdoc.Documentation = {
  "label": "D5110_Display",
  "qualifier": "",
  "summary": [
    {
      "kind": "paragraph",
      "cssClass": "preformatted",
      "children": [
        {
          "kind": "span",
          "text": "    Unit Name:\tD5110_Display\n\n    Contained Units:\n\t\t\tInit\t\t(procedure)\n\t\t\tWrite\t\t(procedure)\n\t\t\tWrite_Char\t(procedure)\n\t\t\tWrite_String\t(procedure)\n\t\t\tClear\t\t(procedure)\n\t\t\tSetXY\t\t(procedure)\n\t\t\tLine\t\t(procedure)\n\t\t\tRectangle\t(procedure)\n\t\t\tCircle\t\t(procedure)\n\t\t\tGraphic\t\t(procedure)\n\n   Description:\n   This unit contains subprograms to write characters, strings and\n   furthermore to draw simple forms and bitmaps to the D5110 display.\n   e.g. for main program snippet:\n\n\tD5110_Display.Init;\n\tD5110_Display.Graphic(graphic_maps.Ada_G);\n------------------------------------------------------------------------------\n\t- Author: Marcus Br.\n\t- Date\t: 28/04/2019 (start of development)\n------------------------------------------------------------------------------\n\n"
        }
      ]
    }
  ],
  "description": [
    {
      "kind": "paragraph",
      "cssClass": "preformatted",
      "children": [
        {
          "kind": "span",
          "text": "\n"
        }
      ]
    }
  ],
  "entities": [
    {
      "entities": [
        {
          "label": "Max_X",
          "qualifier": "",
          "line": 78,
          "column": 4,
          "src": "srcs/d5110_display.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 78,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Max_X"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "constant"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "NOKIA5110_DIMX_T"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":="
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "number",
                      "text": "83"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "label": "Max_Y",
          "qualifier": "",
          "line": 79,
          "column": 4,
          "src": "srcs/d5110_display.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 79,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Max_Y"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "constant"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "NOKIA5110_DIMY_T"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":="
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "number",
                      "text": "47"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            }
          ]
        }
      ],
      "label": "Constants and variables"
    },
    {
      "entities": [
        {
          "label": "D_COMMAND_T",
          "qualifier": "",
          "line": 58,
          "column": 12,
          "src": "srcs/d5110_display.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 58,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "subtype"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "D_COMMAND_T",
                      "href": "docs/d5110_display___spec.html#L58C12"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "is"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_8"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "label": "DISPLAY_BUFFER_TA",
          "qualifier": "",
          "line": 164,
          "column": 9,
          "src": "srcs/d5110_display.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 164,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "type"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "DISPLAY_BUFFER_TA",
                      "href": "docs/d5110_display___spec.html#L164C9"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "is"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "array"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "("
                    },
                    {
                      "kind": "span",
                      "cssClass": "number",
                      "text": "0"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ".."
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "number",
                      "text": "83"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ","
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "number",
                      "text": "0"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ".."
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "number",
                      "text": "5"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ")"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "of"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "D_COMMAND_T",
                      "href": "docs/d5110_display___spec.html#L58C12"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": " datatype represents the display buffer\n\n"
                }
              ]
            }
          ]
        },
        {
          "label": "DISPLAY_CHARACTER_TA",
          "qualifier": "",
          "line": 61,
          "column": 9,
          "src": "srcs/d5110_display.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 61,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "type"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "DISPLAY_CHARACTER_TA",
                      "href": "docs/d5110_display___spec.html#L61C9"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "is"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "array"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "("
                    },
                    {
                      "kind": "span",
                      "cssClass": "number",
                      "text": "0"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ".."
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "number",
                      "text": "95"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ","
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "number",
                      "text": "0"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ".."
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "number",
                      "text": "4"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ")"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "of"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "D_COMMAND_T",
                      "href": "docs/d5110_display___spec.html#L58C12"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": " bitmaps for characters (ASCII code)\n\n"
                }
              ]
            }
          ]
        },
        {
          "label": "MAP_5110_TA",
          "qualifier": "",
          "line": 66,
          "column": 9,
          "src": "srcs/d5110_display.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 66,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "type"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "MAP_5110_TA",
                      "href": "docs/d5110_display___spec.html#L66C9"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "  "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "is"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "array"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "("
                    },
                    {
                      "kind": "span",
                      "cssClass": "number",
                      "text": "0"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ".."
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "number",
                      "text": "503"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ")"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "of"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_8"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": " bitmap type for images 504 * 8 bit\n\n"
                }
              ]
            }
          ]
        },
        {
          "label": "NOKIA5110_DIMX_T",
          "qualifier": "",
          "line": 71,
          "column": 12,
          "src": "srcs/d5110_display.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 71,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "subtype"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "NOKIA5110_DIMX_T",
                      "href": "docs/d5110_display___spec.html#L71C12"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "is"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Natural"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "range"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "number",
                      "text": "0"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ".."
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "number",
                      "text": "83"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": " type for pixel coordinates\n\n"
                }
              ]
            }
          ]
        },
        {
          "label": "NOKIA5110_DIMY_T",
          "qualifier": "",
          "line": 72,
          "column": 12,
          "src": "srcs/d5110_display.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 72,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "subtype"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "NOKIA5110_DIMY_T",
                      "href": "docs/d5110_display___spec.html#L72C12"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "is"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Natural"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "range"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "number",
                      "text": "0"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ".."
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "number",
                      "text": "47"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            }
          ]
        }
      ],
      "label": "Simple types"
    },
    {
      "entities": [
        {
          "label": "Circle",
          "qualifier": "",
          "line": 148,
          "column": 14,
          "src": "srcs/d5110_display.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 148,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "procedure"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Circle",
                      "href": "docs/d5110_display___spec.html#L148C14"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 149,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "     "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "("
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "X",
                      "href": "docs/d5110_display___spec.html#L149C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "in"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "NOKIA5110_DIMX_T",
                      "href": "docs/d5110_display___spec.html#L71C12"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 150,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Y",
                      "href": "docs/d5110_display___spec.html#L150C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "in"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "NOKIA5110_DIMY_T",
                      "href": "docs/d5110_display___spec.html#L72C12"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 151,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "R",
                      "href": "docs/d5110_display___spec.html#L151C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "    "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Positive"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ")"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": " Draws a circle on the display.\n\n"
                }
              ]
            }
          ],
          "parameters": [
            {
              "label": "X",
              "line": 149,
              "column": 7,
              "type": {
                "label": "D5110_Display.NOKIA5110_DIMX_T",
                "docHref": "docs/d5110_display___spec.html#L71C12"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "Y",
              "line": 150,
              "column": 7,
              "type": {
                "label": "D5110_Display.NOKIA5110_DIMY_T",
                "docHref": "docs/d5110_display___spec.html#L72C12"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "R",
              "line": 151,
              "column": 7,
              "type": {
                "label": "Positive"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "label": "Clear",
          "qualifier": "",
          "line": 102,
          "column": 14,
          "src": "srcs/d5110_display.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 102,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "procedure"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Clear",
                      "href": "docs/d5110_display___spec.html#L102C14"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": " Clear the display.\n\n"
                }
              ]
            }
          ]
        },
        {
          "label": "Graphic",
          "qualifier": "",
          "line": 156,
          "column": 14,
          "src": "srcs/d5110_display.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 156,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "procedure"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Graphic",
                      "href": "docs/d5110_display___spec.html#L156C14"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "("
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "G",
                      "href": "docs/d5110_display___spec.html#L156C22"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "in"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "MAP_5110_TA",
                      "href": "docs/d5110_display___spec.html#L66C9"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ")"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": " Draws a bitmap of type MAP_5110_TA on the display.\n\n"
                }
              ]
            }
          ],
          "parameters": [
            {
              "label": "G",
              "line": 156,
              "column": 22,
              "type": {
                "label": "D5110_Display.MAP_5110_TA",
                "docHref": "docs/d5110_display___spec.html#L66C9"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "label": "Init",
          "qualifier": "",
          "line": 124,
          "column": 14,
          "src": "srcs/d5110_display.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 124,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "procedure"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Init",
                      "href": "docs/d5110_display___spec.html#L124C14"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": " Display Init - setting GPIO Pins.\n\n"
                }
              ]
            }
          ]
        },
        {
          "label": "Line",
          "qualifier": "",
          "line": 130,
          "column": 14,
          "src": "srcs/d5110_display.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 130,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "procedure"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Line",
                      "href": "docs/d5110_display___spec.html#L130C14"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 131,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "     "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "("
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "X1",
                      "href": "docs/d5110_display___spec.html#L131C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "in"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "NOKIA5110_DIMX_T",
                      "href": "docs/d5110_display___spec.html#L71C12"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 132,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Y1",
                      "href": "docs/d5110_display___spec.html#L132C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "in"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "NOKIA5110_DIMY_T",
                      "href": "docs/d5110_display___spec.html#L72C12"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 133,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "X2",
                      "href": "docs/d5110_display___spec.html#L133C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "in"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "NOKIA5110_DIMX_T",
                      "href": "docs/d5110_display___spec.html#L71C12"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 134,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Y2",
                      "href": "docs/d5110_display___spec.html#L134C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "in"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "NOKIA5110_DIMY_T",
                      "href": "docs/d5110_display___spec.html#L72C12"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ")"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": " Draws a line on the display.\n\n"
                }
              ]
            }
          ],
          "parameters": [
            {
              "label": "X1",
              "line": 131,
              "column": 7,
              "type": {
                "label": "D5110_Display.NOKIA5110_DIMX_T",
                "docHref": "docs/d5110_display___spec.html#L71C12"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "Y1",
              "line": 132,
              "column": 7,
              "type": {
                "label": "D5110_Display.NOKIA5110_DIMY_T",
                "docHref": "docs/d5110_display___spec.html#L72C12"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "X2",
              "line": 133,
              "column": 7,
              "type": {
                "label": "D5110_Display.NOKIA5110_DIMX_T",
                "docHref": "docs/d5110_display___spec.html#L71C12"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "Y2",
              "line": 134,
              "column": 7,
              "type": {
                "label": "D5110_Display.NOKIA5110_DIMY_T",
                "docHref": "docs/d5110_display___spec.html#L72C12"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "label": "PutPixel",
          "qualifier": "",
          "line": 114,
          "column": 14,
          "src": "srcs/d5110_display.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 114,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "procedure"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "PutPixel",
                      "href": "docs/d5110_display___spec.html#L114C14"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "("
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "X",
                      "href": "docs/d5110_display___spec.html#L114C24"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "in"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "NOKIA5110_DIMX_T",
                      "href": "docs/d5110_display___spec.html#L71C12"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Y",
                      "href": "docs/d5110_display___spec.html#L114C49"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "in"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "NOKIA5110_DIMX_T",
                      "href": "docs/d5110_display___spec.html#L71C12"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ")"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": " Set pixel black at X(0..83) and Y(0..47).\n\n"
                }
              ]
            }
          ],
          "parameters": [
            {
              "label": "X",
              "line": 114,
              "column": 24,
              "type": {
                "label": "D5110_Display.NOKIA5110_DIMX_T",
                "docHref": "docs/d5110_display___spec.html#L71C12"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "Y",
              "line": 114,
              "column": 49,
              "type": {
                "label": "D5110_Display.NOKIA5110_DIMX_T",
                "docHref": "docs/d5110_display___spec.html#L71C12"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "label": "Rectangle",
          "qualifier": "",
          "line": 139,
          "column": 14,
          "src": "srcs/d5110_display.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 139,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "procedure"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Rectangle",
                      "href": "docs/d5110_display___spec.html#L139C14"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 140,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "     "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "("
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "X",
                      "href": "docs/d5110_display___spec.html#L140C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "  "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "in"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "NOKIA5110_DIMX_T",
                      "href": "docs/d5110_display___spec.html#L71C12"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 141,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Y",
                      "href": "docs/d5110_display___spec.html#L141C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "  "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "in"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "NOKIA5110_DIMY_T",
                      "href": "docs/d5110_display___spec.html#L72C12"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 142,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Dx",
                      "href": "docs/d5110_display___spec.html#L142C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "in"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "NOKIA5110_DIMX_T",
                      "href": "docs/d5110_display___spec.html#L71C12"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 143,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Dy",
                      "href": "docs/d5110_display___spec.html#L143C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "in"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "NOKIA5110_DIMY_T",
                      "href": "docs/d5110_display___spec.html#L72C12"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ")"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": " Draws a rectangle on the display.\n\n"
                }
              ]
            }
          ],
          "parameters": [
            {
              "label": "X",
              "line": 140,
              "column": 7,
              "type": {
                "label": "D5110_Display.NOKIA5110_DIMX_T",
                "docHref": "docs/d5110_display___spec.html#L71C12"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "Y",
              "line": 141,
              "column": 7,
              "type": {
                "label": "D5110_Display.NOKIA5110_DIMY_T",
                "docHref": "docs/d5110_display___spec.html#L72C12"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "Dx",
              "line": 142,
              "column": 7,
              "type": {
                "label": "D5110_Display.NOKIA5110_DIMX_T",
                "docHref": "docs/d5110_display___spec.html#L71C12"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "Dy",
              "line": 143,
              "column": 7,
              "type": {
                "label": "D5110_Display.NOKIA5110_DIMY_T",
                "docHref": "docs/d5110_display___spec.html#L72C12"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "label": "SetXY",
          "qualifier": "",
          "line": 108,
          "column": 14,
          "src": "srcs/d5110_display.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 108,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "procedure"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "SetXY",
                      "href": "docs/d5110_display___spec.html#L108C14"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "("
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "X",
                      "href": "docs/d5110_display___spec.html#L108C21"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "in"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_8"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Y",
                      "href": "docs/d5110_display___spec.html#L108C51"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "in"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_8"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ")"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": " Set drawing cursor at X(0..83) and Y(0..47).\n\n"
                }
              ]
            }
          ],
          "parameters": [
            {
              "label": "X",
              "line": 108,
              "column": 21,
              "type": {
                "label": "Unsigned_8"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "Y",
              "line": 108,
              "column": 51,
              "type": {
                "label": "Unsigned_8"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "label": "Update",
          "qualifier": "",
          "line": 119,
          "column": 14,
          "src": "srcs/d5110_display.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 119,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "procedure"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Update",
                      "href": "docs/d5110_display___spec.html#L119C14"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": " Update the display content.\n\n"
                }
              ]
            }
          ]
        },
        {
          "label": "Write",
          "qualifier": "",
          "line": 85,
          "column": 14,
          "src": "srcs/d5110_display.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 85,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "procedure"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Write",
                      "href": "docs/d5110_display___spec.html#L85C14"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "("
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "DC",
                      "href": "docs/d5110_display___spec.html#L85C21"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Gpio.PINVOLTAGE_T",
                      "href": "docs/gpio___spec.html#L61C9"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Data",
                      "href": "docs/d5110_display___spec.html#L85C45"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "in"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "D_COMMAND_T",
                      "href": "docs/d5110_display___spec.html#L58C12"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ")"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": " Writes Data or Command to the Display.\n\n"
                }
              ]
            }
          ],
          "parameters": [
            {
              "label": "DC",
              "line": 85,
              "column": 21,
              "type": {
                "label": "Gpio.PINVOLTAGE_T",
                "docHref": "docs/gpio___spec.html#L61C9"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "Data",
              "line": 85,
              "column": 45,
              "type": {
                "label": "D5110_Display.D_COMMAND_T",
                "docHref": "docs/d5110_display___spec.html#L58C12"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "label": "Write_Char",
          "qualifier": "",
          "line": 90,
          "column": 14,
          "src": "srcs/d5110_display.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 90,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "procedure"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Write_Char",
                      "href": "docs/d5110_display___spec.html#L90C14"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "("
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "C",
                      "href": "docs/d5110_display___spec.html#L90C26"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "in"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Character"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ")"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": " Writes character on the display.\n\n"
                }
              ]
            }
          ],
          "parameters": [
            {
              "label": "C",
              "line": 90,
              "column": 26,
              "type": {
                "label": "Character"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "label": "Write_String",
          "qualifier": "",
          "line": 95,
          "column": 14,
          "src": "srcs/d5110_display.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 95,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "procedure"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Write_String",
                      "href": "docs/d5110_display___spec.html#L95C14"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "("
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "S",
                      "href": "docs/d5110_display___spec.html#L95C28"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "in"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "String"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ")"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": " Writes string on the display.\n\n"
                }
              ]
            }
          ],
          "parameters": [
            {
              "label": "S",
              "line": 95,
              "column": 28,
              "type": {
                "label": "String"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            }
          ]
        }
      ],
      "label": "Subprograms"
    }
  ]
};