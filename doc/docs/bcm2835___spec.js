GNATdoc.Documentation = {
  "label": "Bcm2835",
  "qualifier": "",
  "summary": [
    {
      "kind": "paragraph",
      "cssClass": "preformatted",
      "children": [
        {
          "kind": "span",
          "text": "    Unit Name:\tBcm2835\n\n    Datatypes:\n\t\t\tSYSTEM_TIMER_REG_TR\t(record)\n\t\t\tSPI_REG_TR\t\t(record)\n\t\t\tGPIO_REG_TR\t\t(record)\n\n    Constants:\n\t\t\tBCM2835_Peri_Base = 16#20000000#\n\t\t\tBCM2835_GPIO_Base = 16#20200000#\n\t\n    Variables:\n\t\t\tGPIO_Reg \t: GPIO_REG_TR\n\t\t\tSPI_R \t\t: SPI_REG_TR;\n\t\t\tS_TIMER \t: SYSTEM_TIMER_REG_TR;\n\n   Description:\n   This unit contains the constants and memory mapped variables to address\n   the general purpose input/ouput pins and the timer of the BCM2835.\t\n------------------------------------------------------------------------------\n\t- Author: Marcus Br.\n\t- Date\t: 28/04/2019 (start of development)\n------------------------------------------------------------------------------\n\n"
        }
      ]
    }
  ],
  "description": [
    {
      "kind": "paragraph",
      "cssClass": "preformatted",
      "children": [
        {
          "kind": "span",
          "text": "\n"
        }
      ]
    }
  ],
  "entities": [
    {
      "entities": [
        {
          "label": "BCM2835_GPIO_Base",
          "qualifier": "",
          "line": 183,
          "column": 4,
          "src": "srcs/bcm2835.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 183,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "BCM2835_GPIO_Base"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "constant"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":="
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "number",
                      "text": "16"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "#"
                    },
                    {
                      "kind": "span",
                      "cssClass": "number",
                      "text": "20200000"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "#"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": " Base address of General Purpose IO Pins\n\n"
                }
              ]
            }
          ]
        },
        {
          "label": "BCM2835_Peri_Base",
          "qualifier": "",
          "line": 180,
          "column": 4,
          "src": "srcs/bcm2835.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 180,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "BCM2835_Peri_Base"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "constant"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":="
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "number",
                      "text": "16"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "#"
                    },
                    {
                      "kind": "span",
                      "cssClass": "number",
                      "text": "20000000"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "#"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": " Base address of peripherals\n\n"
                }
              ]
            }
          ]
        },
        {
          "label": "FR_REG",
          "qualifier": "",
          "line": 207,
          "column": 4,
          "src": "srcs/bcm2835.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 207,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "FR_REG"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "B32_REG_TA"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "label": "GPIO_Reg",
          "qualifier": "",
          "line": 189,
          "column": 4,
          "src": "srcs/bcm2835.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 189,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "GPIO_Reg"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "GPIO_REG_TR"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "label": "S_TIMER",
          "qualifier": "",
          "line": 198,
          "column": 4,
          "src": "srcs/bcm2835.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 198,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "S_TIMER"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "SYSTEM_TIMER_REG_TR"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "label": "SPI_R",
          "qualifier": "",
          "line": 193,
          "column": 4,
          "src": "srcs/bcm2835.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 193,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "SPI_R"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "SPI_REG_TR"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "label": "UART_REG",
          "qualifier": "",
          "line": 203,
          "column": 4,
          "src": "srcs/bcm2835.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 203,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "UART_REG"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "UART_REG_TR"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            }
          ]
        }
      ],
      "label": "Constants and variables"
    },
    {
      "entities": [
        {
          "label": "B32_REG_TA",
          "qualifier": "",
          "line": 58,
          "column": 9,
          "src": "srcs/bcm2835.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 58,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "type"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "B32_REG_TA",
                      "href": "docs/bcm2835___spec.html#L58C9"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "is"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "array"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "("
                    },
                    {
                      "kind": "span",
                      "cssClass": "number",
                      "text": "1"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ".."
                    },
                    {
                      "kind": "span",
                      "cssClass": "number",
                      "text": "32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ")"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "of"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "BIT_T",
                      "href": "docs/bcm2835___spec.html#L56C12"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": "------------------------------------------------------------------------------\t\n SYSTEM TIMER REGISTER --\n------------------------------------------------------------------------------\n  SYSTEM TIMER REGISTER\n      \n"
                }
              ]
            }
          ]
        },
        {
          "label": "BIT_T",
          "qualifier": "",
          "line": 56,
          "column": 12,
          "src": "srcs/bcm2835.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 56,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "subtype"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "BIT_T",
                      "href": "docs/bcm2835___spec.html#L56C12"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "is"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Natural"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "range"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "number",
                      "text": "0"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ".."
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "number",
                      "text": "1"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": "------------------------------------------------------------------------------\n Datatypes --\n------------------------------------------------------------------------------\n\n"
                }
              ]
            }
          ]
        }
      ],
      "label": "Simple types"
    },
    {
      "entities": [
        {
          "label": "GPIO_REG_TR",
          "qualifier": "",
          "line": 130,
          "column": 9,
          "src": "srcs/bcm2835.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 130,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "type"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "GPIO_REG_TR",
                      "href": "docs/bcm2835___spec.html#L130C9"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "is"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "record"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 131,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "GPFSEL0",
                      "href": "docs/bcm2835___spec.html#L131C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 132,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "GPFSEL1",
                      "href": "docs/bcm2835___spec.html#L132C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 133,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "GPFSEL2",
                      "href": "docs/bcm2835___spec.html#L133C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 134,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "GPFSEL3",
                      "href": "docs/bcm2835___spec.html#L134C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 135,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "GPFSEL4",
                      "href": "docs/bcm2835___spec.html#L135C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 136,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "GPFSEL5",
                      "href": "docs/bcm2835___spec.html#L136C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 137,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Res1",
                      "href": "docs/bcm2835___spec.html#L137C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 138,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "GPSET0",
                      "href": "docs/bcm2835___spec.html#L138C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "    "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 139,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "GPSET1",
                      "href": "docs/bcm2835___spec.html#L139C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "    "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 140,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Res2",
                      "href": "docs/bcm2835___spec.html#L140C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 141,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "GPCLR0",
                      "href": "docs/bcm2835___spec.html#L141C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "    "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 142,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "GPCLR1",
                      "href": "docs/bcm2835___spec.html#L142C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "    "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 143,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Res3",
                      "href": "docs/bcm2835___spec.html#L143C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 144,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "GPLEV0",
                      "href": "docs/bcm2835___spec.html#L144C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "    "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 145,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "GPLEV1",
                      "href": "docs/bcm2835___spec.html#L145C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "    "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 146,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Res4",
                      "href": "docs/bcm2835___spec.html#L146C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 147,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "GPEDS0",
                      "href": "docs/bcm2835___spec.html#L147C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "    "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 148,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "GPEDS1",
                      "href": "docs/bcm2835___spec.html#L148C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "    "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 149,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Res5",
                      "href": "docs/bcm2835___spec.html#L149C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 150,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "GPREN0",
                      "href": "docs/bcm2835___spec.html#L150C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "    "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 151,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "GPREN1",
                      "href": "docs/bcm2835___spec.html#L151C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "    "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 152,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Res6",
                      "href": "docs/bcm2835___spec.html#L152C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 153,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "GPFEN0",
                      "href": "docs/bcm2835___spec.html#L153C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "    "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 154,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "GPFEN1",
                      "href": "docs/bcm2835___spec.html#L154C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "    "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 155,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Res7",
                      "href": "docs/bcm2835___spec.html#L155C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 156,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "GPHEN0",
                      "href": "docs/bcm2835___spec.html#L156C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "    "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 157,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "GPHEN1",
                      "href": "docs/bcm2835___spec.html#L157C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "    "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 158,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Res8",
                      "href": "docs/bcm2835___spec.html#L158C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 159,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "GPLEN0",
                      "href": "docs/bcm2835___spec.html#L159C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "    "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 160,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "GPLEN1",
                      "href": "docs/bcm2835___spec.html#L160C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "    "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 161,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Res9",
                      "href": "docs/bcm2835___spec.html#L161C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 162,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "GPAREN0",
                      "href": "docs/bcm2835___spec.html#L162C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 163,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "GPAREN1",
                      "href": "docs/bcm2835___spec.html#L163C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 164,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Res10",
                      "href": "docs/bcm2835___spec.html#L164C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "     "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 165,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "GPAFEN0",
                      "href": "docs/bcm2835___spec.html#L165C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 166,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "GPAFEN1",
                      "href": "docs/bcm2835___spec.html#L166C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 167,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Res11",
                      "href": "docs/bcm2835___spec.html#L167C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "     "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 168,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "GPPUD",
                      "href": "docs/bcm2835___spec.html#L168C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "     "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 169,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "GPPUDCLK0",
                      "href": "docs/bcm2835___spec.html#L169C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 170,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "GPPUDCLK1",
                      "href": "docs/bcm2835___spec.html#L170C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 171,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Res12",
                      "href": "docs/bcm2835___spec.html#L171C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "     "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 172,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "end"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "record"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";",
                      "href": "docs/bcm2835___spec.html#L130C9"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": "----------------------------------------------------------------------\t\n GPIO REGISTER--\n----------------------------------------------------------------------\n  General Purpose IO Pin REGISTER\n  There are 54 general-purpose I/O (GPIO) lines split into two banks. All GPIO pins have at\n  least two alternative functions within BCM. The alternate functions are usually peripheral IO\n  and  a  single  peripheral  may  appear  in  each  bank  to  allow  flexibility  on  the  choice  of  IO\n  voltage.\n\n"
                }
              ]
            }
          ],
          "fields": [
            {
              "label": "GPFSEL0",
              "line": 131,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "GPFSEL1",
              "line": 132,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "GPFSEL2",
              "line": 133,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "GPFSEL3",
              "line": 134,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "GPFSEL4",
              "line": 135,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "GPFSEL5",
              "line": 136,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "Res1",
              "line": 137,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "GPSET0",
              "line": 138,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "GPSET1",
              "line": 139,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "Res2",
              "line": 140,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "GPCLR0",
              "line": 141,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "GPCLR1",
              "line": 142,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "Res3",
              "line": 143,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "GPLEV0",
              "line": 144,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "GPLEV1",
              "line": 145,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "Res4",
              "line": 146,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "GPEDS0",
              "line": 147,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "GPEDS1",
              "line": 148,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "Res5",
              "line": 149,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "GPREN0",
              "line": 150,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "GPREN1",
              "line": 151,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "Res6",
              "line": 152,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "GPFEN0",
              "line": 153,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "GPFEN1",
              "line": 154,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "Res7",
              "line": 155,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "GPHEN0",
              "line": 156,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "GPHEN1",
              "line": 157,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "Res8",
              "line": 158,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "GPLEN0",
              "line": 159,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "GPLEN1",
              "line": 160,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "Res9",
              "line": 161,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "GPAREN0",
              "line": 162,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "GPAREN1",
              "line": 163,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "Res10",
              "line": 164,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "GPAFEN0",
              "line": 165,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "GPAFEN1",
              "line": 166,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "Res11",
              "line": 167,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "GPPUD",
              "line": 168,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "GPPUDCLK0",
              "line": 169,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "GPPUDCLK1",
              "line": 170,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "Res12",
              "line": 171,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "label": "SPI_REG_TR",
          "qualifier": "",
          "line": 89,
          "column": 9,
          "src": "srcs/bcm2835.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 89,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "type"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "SPI_REG_TR",
                      "href": "docs/bcm2835___spec.html#L89C9"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "is"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "record"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 90,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "CS",
                      "href": "docs/bcm2835___spec.html#L90C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 91,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "FIFO",
                      "href": "docs/bcm2835___spec.html#L91C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 92,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "CLK",
                      "href": "docs/bcm2835___spec.html#L92C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "  "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 93,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "DLEN",
                      "href": "docs/bcm2835___spec.html#L93C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 94,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "LTOH",
                      "href": "docs/bcm2835___spec.html#L94C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 95,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "DC",
                      "href": "docs/bcm2835___spec.html#L95C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 96,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "end"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "record"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";",
                      "href": "docs/bcm2835___spec.html#L89C9"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": "------------------------------------------------------------------------------\t\n SPI REGISTER--\n------------------------------------------------------------------------------\n  Serial Peripheral Interface REGISTER\n \n"
                }
              ]
            }
          ],
          "fields": [
            {
              "label": "CS",
              "line": 90,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "  SPI Master Control and Status\n \n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "FIFO",
              "line": 91,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "  SPI Master TX and RX FIFOs\n \n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "CLK",
              "line": 92,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "  SPI Master Clock Divider\n \n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "DLEN",
              "line": 93,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "  SPI Master Data Length\n \n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "LTOH",
              "line": 94,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "  SPI LOSSI mode TOH\n \n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "DC",
              "line": 95,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "SPI DMA DREQ Controls\n"
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "label": "SYSTEM_TIMER_REG_TR",
          "qualifier": "",
          "line": 69,
          "column": 9,
          "src": "srcs/bcm2835.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 69,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "type"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "SYSTEM_TIMER_REG_TR",
                      "href": "docs/bcm2835___spec.html#L69C9"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "is"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "record"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 70,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "CS",
                      "href": "docs/bcm2835___spec.html#L70C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 71,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "C",
                      "href": "docs/bcm2835___spec.html#L71C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "  "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_64"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 72,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "C0",
                      "href": "docs/bcm2835___spec.html#L72C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 73,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "C1",
                      "href": "docs/bcm2835___spec.html#L73C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 74,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "C2",
                      "href": "docs/bcm2835___spec.html#L74C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 75,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "C3",
                      "href": "docs/bcm2835___spec.html#L75C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 76,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "end"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "record"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";",
                      "href": "docs/bcm2835___spec.html#L69C9"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": "\n"
                }
              ]
            }
          ],
          "fields": [
            {
              "label": "CS",
              "line": 70,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "C",
              "line": 71,
              "column": 7,
              "type": {
                "label": "Unsigned_64"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "C0",
              "line": 72,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "C1",
              "line": 73,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "C2",
              "line": 74,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "C3",
              "line": 75,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "label": "UART_REG_TR",
          "qualifier": "",
          "line": 101,
          "column": 9,
          "src": "srcs/bcm2835.ads.html",
          "summary": [
          ],
          "description": [
            {
              "kind": "code",
              "children": [
                {
                  "kind": "line",
                  "number": 101,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "type"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "UART_REG_TR",
                      "href": "docs/bcm2835___spec.html#L101C9"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "is"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "record"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 102,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "DR",
                      "href": "docs/bcm2835___spec.html#L102C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "     "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 103,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "RSRECR",
                      "href": "docs/bcm2835___spec.html#L103C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 104,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "FR",
                      "href": "docs/bcm2835___spec.html#L104C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "     "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 105,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "ILPR",
                      "href": "docs/bcm2835___spec.html#L105C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 106,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "IBRD",
                      "href": "docs/bcm2835___spec.html#L106C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 107,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "FBRD",
                      "href": "docs/bcm2835___spec.html#L107C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 108,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "LCRH",
                      "href": "docs/bcm2835___spec.html#L108C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 109,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "CR",
                      "href": "docs/bcm2835___spec.html#L109C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "     "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 110,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "IFLS",
                      "href": "docs/bcm2835___spec.html#L110C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 111,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "IMSC",
                      "href": "docs/bcm2835___spec.html#L111C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 112,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "RIS",
                      "href": "docs/bcm2835___spec.html#L112C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "    "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 113,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "MIS",
                      "href": "docs/bcm2835___spec.html#L113C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "    "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 114,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "ICR",
                      "href": "docs/bcm2835___spec.html#L114C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "    "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 115,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "DMACR",
                      "href": "docs/bcm2835___spec.html#L115C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "  "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 116,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "ITCR",
                      "href": "docs/bcm2835___spec.html#L116C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 117,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "ITIP",
                      "href": "docs/bcm2835___spec.html#L117C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 118,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "ITOP",
                      "href": "docs/bcm2835___spec.html#L118C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 119,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "      "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "TDR",
                      "href": "docs/bcm2835___spec.html#L119C7"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "    "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ":"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": "Interfaces.Unsigned_32"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";"
                    }
                  ]
                },
                {
                  "kind": "line",
                  "number": 120,
                  "children": [
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": "   "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "end"
                    },
                    {
                      "kind": "span",
                      "cssClass": "text",
                      "text": " "
                    },
                    {
                      "kind": "span",
                      "cssClass": "keyword",
                      "text": "record"
                    },
                    {
                      "kind": "span",
                      "cssClass": "identifier",
                      "text": ";",
                      "href": "docs/bcm2835___spec.html#L101C9"
                    }
                  ]
                }
              ]
            },
            {
              "kind": "paragraph",
              "cssClass": "preformatted",
              "children": [
                {
                  "kind": "span",
                  "text": "------------------------------------------------------------------------------\t\n UART REGISTER--\n------------------------------------------------------------------------------\n\n"
                }
              ]
            }
          ],
          "fields": [
            {
              "label": "DR",
              "line": 102,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "RSRECR",
              "line": 103,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "FR",
              "line": 104,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "ILPR",
              "line": 105,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "IBRD",
              "line": 106,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "FBRD",
              "line": 107,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "LCRH",
              "line": 108,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "CR",
              "line": 109,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "IFLS",
              "line": 110,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "IMSC",
              "line": 111,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "RIS",
              "line": 112,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "MIS",
              "line": 113,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "ICR",
              "line": 114,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "DMACR",
              "line": 115,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "ITCR",
              "line": 116,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "ITIP",
              "line": 117,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "ITOP",
              "line": 118,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            },
            {
              "label": "TDR",
              "line": 119,
              "column": 7,
              "type": {
                "label": "Unsigned_32"
              },
              "description": [
                {
                  "kind": "paragraph",
                  "cssClass": "preformatted",
                  "children": [
                    {
                      "kind": "span",
                      "text": "\n"
                    }
                  ]
                }
              ]
            }
          ]
        }
      ],
      "label": "Record types"
    }
  ]
};