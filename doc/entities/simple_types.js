GNATdoc.EntitiesCategory = {
  "label": "Simple Types",
  "entities": [
    {
      "label": "B32_REG_TA",
      "docHref": "docs/bcm2835___spec.html#L58C9",
      "declared": "Bcm2835",
      "declared_qualifier": "",
      "srcHref": "srcs/bcm2835.ads.html#L58"
    },
    {
      "label": "BIT_T",
      "docHref": "docs/bcm2835___spec.html#L56C12",
      "declared": "Bcm2835",
      "declared_qualifier": "",
      "srcHref": "srcs/bcm2835.ads.html#L56"
    },
    {
      "label": "D_COMMAND_T",
      "docHref": "docs/d5110_display___spec.html#L58C12",
      "declared": "D5110_Display",
      "declared_qualifier": "",
      "srcHref": "srcs/d5110_display.ads.html#L58"
    },
    {
      "label": "DISPLAY_BUFFER_TA",
      "docHref": "docs/d5110_display___spec.html#L164C9",
      "declared": "D5110_Display",
      "declared_qualifier": "",
      "srcHref": "srcs/d5110_display.ads.html#L164"
    },
    {
      "label": "DISPLAY_CHARACTER_TA",
      "docHref": "docs/d5110_display___spec.html#L61C9",
      "declared": "D5110_Display",
      "declared_qualifier": "",
      "srcHref": "srcs/d5110_display.ads.html#L61"
    },
    {
      "label": "MAP_5110_TA",
      "docHref": "docs/d5110_display___spec.html#L66C9",
      "declared": "D5110_Display",
      "declared_qualifier": "",
      "srcHref": "srcs/d5110_display.ads.html#L66"
    },
    {
      "label": "NOKIA5110_DIMX_T",
      "docHref": "docs/d5110_display___spec.html#L71C12",
      "declared": "D5110_Display",
      "declared_qualifier": "",
      "srcHref": "srcs/d5110_display.ads.html#L71"
    },
    {
      "label": "NOKIA5110_DIMY_T",
      "docHref": "docs/d5110_display___spec.html#L72C12",
      "declared": "D5110_Display",
      "declared_qualifier": "",
      "srcHref": "srcs/d5110_display.ads.html#L72"
    },
    {
      "label": "PINVOLTAGE_T",
      "docHref": "docs/gpio___spec.html#L61C9",
      "declared": "Gpio",
      "declared_qualifier": "",
      "srcHref": "srcs/gpio.ads.html#L61"
    }
  ]
};