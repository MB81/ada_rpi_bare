GNATdoc.EntitiesCategory = {
  "label": "Subprograms",
  "entities": [
    {
      "label": "Blink",
      "docHref": "docs/gpio___spec.html#L110C14",
      "declared": "Gpio",
      "declared_qualifier": "",
      "srcHref": "srcs/gpio.ads.html#L110"
    },
    {
      "label": "Circle",
      "docHref": "docs/d5110_display___spec.html#L148C14",
      "declared": "D5110_Display",
      "declared_qualifier": "",
      "srcHref": "srcs/d5110_display.ads.html#L148"
    },
    {
      "label": "Clear",
      "docHref": "docs/d5110_display___spec.html#L102C14",
      "declared": "D5110_Display",
      "declared_qualifier": "",
      "srcHref": "srcs/d5110_display.ads.html#L102"
    },
    {
      "label": "GetC",
      "docHref": "docs/uart___spec.html#L73C13",
      "declared": "Uart",
      "declared_qualifier": "",
      "srcHref": "srcs/uart.ads.html#L73"
    },
    {
      "label": "Graphic",
      "docHref": "docs/d5110_display___spec.html#L156C14",
      "declared": "D5110_Display",
      "declared_qualifier": "",
      "srcHref": "srcs/d5110_display.ads.html#L156"
    },
    {
      "label": "Init",
      "docHref": "docs/d5110_display___spec.html#L124C14",
      "declared": "D5110_Display",
      "declared_qualifier": "",
      "srcHref": "srcs/d5110_display.ads.html#L124"
    },
    {
      "label": "Init",
      "docHref": "docs/uart___spec.html#L65C14",
      "declared": "Uart",
      "declared_qualifier": "",
      "srcHref": "srcs/uart.ads.html#L65"
    },
    {
      "label": "Init_SPI",
      "docHref": "docs/gpio___spec.html#L116C14",
      "declared": "Gpio",
      "declared_qualifier": "",
      "srcHref": "srcs/gpio.ads.html#L116"
    },
    {
      "label": "Line",
      "docHref": "docs/d5110_display___spec.html#L130C14",
      "declared": "D5110_Display",
      "declared_qualifier": "",
      "srcHref": "srcs/d5110_display.ads.html#L130"
    },
    {
      "label": "NewLine",
      "docHref": "docs/uart___spec.html#L81C14",
      "declared": "Uart",
      "declared_qualifier": "",
      "srcHref": "srcs/uart.ads.html#L81"
    },
    {
      "label": "PutC",
      "docHref": "docs/uart___spec.html#L69C14",
      "declared": "Uart",
      "declared_qualifier": "",
      "srcHref": "srcs/uart.ads.html#L69"
    },
    {
      "label": "PutLine",
      "docHref": "docs/uart___spec.html#L77C14",
      "declared": "Uart",
      "declared_qualifier": "",
      "srcHref": "srcs/uart.ads.html#L77"
    },
    {
      "label": "PutPixel",
      "docHref": "docs/d5110_display___spec.html#L114C14",
      "declared": "D5110_Display",
      "declared_qualifier": "",
      "srcHref": "srcs/d5110_display.ads.html#L114"
    },
    {
      "label": "Rectangle",
      "docHref": "docs/d5110_display___spec.html#L139C14",
      "declared": "D5110_Display",
      "declared_qualifier": "",
      "srcHref": "srcs/d5110_display.ads.html#L139"
    },
    {
      "label": "Set_GPIOF",
      "docHref": "docs/gpio___spec.html#L95C14",
      "declared": "Gpio",
      "declared_qualifier": "",
      "srcHref": "srcs/gpio.ads.html#L95"
    },
    {
      "label": "Set_Volt",
      "docHref": "docs/gpio___spec.html#L104C14",
      "declared": "Gpio",
      "declared_qualifier": "",
      "srcHref": "srcs/gpio.ads.html#L104"
    },
    {
      "label": "SetXY",
      "docHref": "docs/d5110_display___spec.html#L108C14",
      "declared": "D5110_Display",
      "declared_qualifier": "",
      "srcHref": "srcs/d5110_display.ads.html#L108"
    },
    {
      "label": "ToString",
      "docHref": "docs/uart___spec.html#L85C14",
      "declared": "Uart",
      "declared_qualifier": "",
      "srcHref": "srcs/uart.ads.html#L85"
    },
    {
      "label": "Update",
      "docHref": "docs/d5110_display___spec.html#L119C14",
      "declared": "D5110_Display",
      "declared_qualifier": "",
      "srcHref": "srcs/d5110_display.ads.html#L119"
    },
    {
      "label": "Wait",
      "docHref": "docs/gpio___spec.html#L90C14",
      "declared": "Gpio",
      "declared_qualifier": "",
      "srcHref": "srcs/gpio.ads.html#L90"
    },
    {
      "label": "Write",
      "docHref": "docs/d5110_display___spec.html#L85C14",
      "declared": "D5110_Display",
      "declared_qualifier": "",
      "srcHref": "srcs/d5110_display.ads.html#L85"
    },
    {
      "label": "Write_Char",
      "docHref": "docs/d5110_display___spec.html#L90C14",
      "declared": "D5110_Display",
      "declared_qualifier": "",
      "srcHref": "srcs/d5110_display.ads.html#L90"
    },
    {
      "label": "Write_String",
      "docHref": "docs/d5110_display___spec.html#L95C14",
      "declared": "D5110_Display",
      "declared_qualifier": "",
      "srcHref": "srcs/d5110_display.ads.html#L95"
    }
  ]
};