GNATdoc.EntitiesCategory = {
  "label": "Record Types",
  "entities": [
    {
      "label": "GPIO_REG_TR",
      "docHref": "docs/bcm2835___spec.html#L130C9",
      "declared": "Bcm2835",
      "declared_qualifier": "",
      "srcHref": "srcs/bcm2835.ads.html#L130"
    },
    {
      "label": "SPI_REG_TR",
      "docHref": "docs/bcm2835___spec.html#L89C9",
      "declared": "Bcm2835",
      "declared_qualifier": "",
      "srcHref": "srcs/bcm2835.ads.html#L89"
    },
    {
      "label": "SYSTEM_TIMER_REG_TR",
      "docHref": "docs/bcm2835___spec.html#L69C9",
      "declared": "Bcm2835",
      "declared_qualifier": "",
      "srcHref": "srcs/bcm2835.ads.html#L69"
    },
    {
      "label": "UART_REG_TR",
      "docHref": "docs/bcm2835___spec.html#L101C9",
      "declared": "Bcm2835",
      "declared_qualifier": "",
      "srcHref": "srcs/bcm2835.ads.html#L101"
    }
  ]
};