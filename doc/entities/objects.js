GNATdoc.EntitiesCategory = {
  "label": "Constants & Variables",
  "entities": [
    {
      "label": "A0",
      "docHref": "docs/gpio___spec.html#L74C4",
      "declared": "Gpio",
      "declared_qualifier": "",
      "srcHref": "srcs/gpio.ads.html#L74"
    },
    {
      "label": "A1",
      "docHref": "docs/gpio___spec.html#L76C4",
      "declared": "Gpio",
      "declared_qualifier": "",
      "srcHref": "srcs/gpio.ads.html#L76"
    },
    {
      "label": "A2",
      "docHref": "docs/gpio___spec.html#L78C4",
      "declared": "Gpio",
      "declared_qualifier": "",
      "srcHref": "srcs/gpio.ads.html#L78"
    },
    {
      "label": "A3",
      "docHref": "docs/gpio___spec.html#L80C4",
      "declared": "Gpio",
      "declared_qualifier": "",
      "srcHref": "srcs/gpio.ads.html#L80"
    },
    {
      "label": "A4",
      "docHref": "docs/gpio___spec.html#L82C4",
      "declared": "Gpio",
      "declared_qualifier": "",
      "srcHref": "srcs/gpio.ads.html#L82"
    },
    {
      "label": "A5",
      "docHref": "docs/gpio___spec.html#L84C4",
      "declared": "Gpio",
      "declared_qualifier": "",
      "srcHref": "srcs/gpio.ads.html#L84"
    },
    {
      "label": "Ada_G",
      "docHref": "docs/graphic_maps___spec.html#L113C4",
      "declared": "Graphic_Maps",
      "declared_qualifier": "",
      "srcHref": "srcs/graphic_maps.ads.html#L113"
    },
    {
      "label": "ASCII_Table",
      "docHref": "docs/graphic_maps___spec.html#L149C4",
      "declared": "Graphic_Maps",
      "declared_qualifier": "",
      "srcHref": "srcs/graphic_maps.ads.html#L149"
    },
    {
      "label": "BCM2835_GPIO_Base",
      "docHref": "docs/bcm2835___spec.html#L183C4",
      "declared": "Bcm2835",
      "declared_qualifier": "",
      "srcHref": "srcs/bcm2835.ads.html#L183"
    },
    {
      "label": "BCM2835_Peri_Base",
      "docHref": "docs/bcm2835___spec.html#L180C4",
      "declared": "Bcm2835",
      "declared_qualifier": "",
      "srcHref": "srcs/bcm2835.ads.html#L180"
    },
    {
      "label": "BMW_G",
      "docHref": "docs/graphic_maps___spec.html#L78C4",
      "declared": "Graphic_Maps",
      "declared_qualifier": "",
      "srcHref": "srcs/graphic_maps.ads.html#L78"
    },
    {
      "label": "FR_REG",
      "docHref": "docs/bcm2835___spec.html#L207C4",
      "declared": "Bcm2835",
      "declared_qualifier": "",
      "srcHref": "srcs/bcm2835.ads.html#L207"
    },
    {
      "label": "GPIO_Reg",
      "docHref": "docs/bcm2835___spec.html#L189C4",
      "declared": "Bcm2835",
      "declared_qualifier": "",
      "srcHref": "srcs/bcm2835.ads.html#L189"
    },
    {
      "label": "INPUT",
      "docHref": "docs/gpio___spec.html#L70C4",
      "declared": "Gpio",
      "declared_qualifier": "",
      "srcHref": "srcs/gpio.ads.html#L70"
    },
    {
      "label": "Max_X",
      "docHref": "docs/d5110_display___spec.html#L78C4",
      "declared": "D5110_Display",
      "declared_qualifier": "",
      "srcHref": "srcs/d5110_display.ads.html#L78"
    },
    {
      "label": "Max_Y",
      "docHref": "docs/d5110_display___spec.html#L79C4",
      "declared": "D5110_Display",
      "declared_qualifier": "",
      "srcHref": "srcs/d5110_display.ads.html#L79"
    },
    {
      "label": "OUTPUT",
      "docHref": "docs/gpio___spec.html#L72C4",
      "declared": "Gpio",
      "declared_qualifier": "",
      "srcHref": "srcs/gpio.ads.html#L72"
    },
    {
      "label": "S_TIMER",
      "docHref": "docs/bcm2835___spec.html#L198C4",
      "declared": "Bcm2835",
      "declared_qualifier": "",
      "srcHref": "srcs/bcm2835.ads.html#L198"
    },
    {
      "label": "Skull_G",
      "docHref": "docs/graphic_maps___spec.html#L44C4",
      "declared": "Graphic_Maps",
      "declared_qualifier": "",
      "srcHref": "srcs/graphic_maps.ads.html#L44"
    },
    {
      "label": "SPI_R",
      "docHref": "docs/bcm2835___spec.html#L193C4",
      "declared": "Bcm2835",
      "declared_qualifier": "",
      "srcHref": "srcs/bcm2835.ads.html#L193"
    },
    {
      "label": "UART_REG",
      "docHref": "docs/bcm2835___spec.html#L203C4",
      "declared": "Bcm2835",
      "declared_qualifier": "",
      "srcHref": "srcs/bcm2835.ads.html#L203"
    }
  ]
};