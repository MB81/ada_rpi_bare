# Raspberry Pi 1 (Model B 12.2011)

## Project Description:

Little project to get code (written in Ada) running bare metal on a Raspberry Pi 1.

based on documentation provided [here](https://www.raspberrypi.org/documentation/hardware/raspberrypi/bcm2835/README.md).

- [configure](Makefile) gcc and ld , result must be kernel.img which [runs](src/main.adb) without underlaying os and without Ada runtime
- write some packages for [base](src/bcm2835.ads)- and [GPIO](src/gpio.ads)-register
- write a driver to communicate with a LCD Display([Nokia 5110](src/d5110_display.ads)) via SPI - protocol
- write a driver for the [UART](src/uart.ads) - interface to provide text output to an terminal
- write a library to get some graphics output via HDMI

## Examples

**See the wiki.**

## Development Environment:

![](./images/Bild1.png)

#### Hardware:

- PC x64
- Raspberry Pi 1 (Model B 12.2011)

#### Software:

- Windows 10 Pro - GNAT Programming Studio 2018
- Qemu 1.1.0
- 2012-07-15-wheezy-raspbian <http://ftp.jaist.ac.jp/pub/raspberrypi/raspbian/images/>



### Compile & Link:

- Host: Win10 64bit
- Guest: Quemu ([2012-07-15-wheezy-raspbian.img](http://ftp.jaist.ac.jp/pub/raspberrypi/raspbian/images/2012-07-15-wheezy-raspbian/2012-07-15-wheezy-raspbian.zip))



###### 1. Start Qemu guest (qemu/runQemu.bat):

```shell 
qemu-system-arm.exe -monitor stdio -cpu arm1176 -kernel kernel-qemu -append "root=/dev/sda2" -hda 2012-07-15-wheezy-raspbian.img -m 512 -M versatilepb -redir tcp:2222::22 
```

###### 2. Copy sources(*.adb, *.ads) to Qemu e.g. with Filezilla

###### 3. Compile and link a **kernel.img**

- place the Makefile next to your Ada sources
- `make kernel`

###### 4. Copy kernel.img to SD Card

###### 5. Boot Raspi with SD-Card



### Image Attribution:

Raspberry Image: By Efa at English Wikipedia, CC BY-SA 3.0, https://commons.wikimedia.org/w/index.php?curid=53283176