TARGET: kernel
### Toolchain ### 
CC = gcc
AS = as
LD = ld
RM = rm

### Ada flags ### 
GNATFLAGS = -gnat95 -gnatN -gnatE -gnatf

### gcc flags### 
CFLAGS =-O2 -marm -march=armv6zk -mcpu=arm1176jzf-s -mfpu=vfp -mfloat-abi=hard  $(GNATFLAGS) -r -fpack-struct -nostartfiles -static -fno-toplevel-reorder -fno-jump-tables -falign-jumps -fno-defer-pop 

### linker flags ### 
LDFLAGS =-AArmv6zk -e 0x8000 -q -T startup.ld -nostdlib -nostdincl -static --no-undefined

### assembler flags ### 
ASFLAGS=-march=armv6zk -mcpu=arm1176jzf-s -mfpu=vfp -mfloat-abi=hard

### directories ### 
ODIR = obj/
SDIR = src/

### object files to be generated ### 
OBJ = start.o gpio.o spi.o bcm2835.o d5110_display.o graphic_maps.o uart.o main.o

### DO NOT CHANGE BELOW ### 
kernel: $(OBJ)
	$(LD) $(addprefix $(ODIR),$^) $(LDFLAGS) -o $(ODIR)kernel.o
	objcopy $(ODIR)kernel.o -O binary kernel.img

start.o: $(SDIR)start.s
	$(AS) -o $(ODIR)/$@ $< $(ASFLAGS)

%.o:  $(SDIR)%.adb
	$(CC) $(CFLAGS) -c $< -o $(ODIR)$@
%.o: $(SDIR)%.ads
	$(CC) $(CFLAGS) -c $< -o $(ODIR)$@	 

clean:
	$(RM) $(ODIR)*.*